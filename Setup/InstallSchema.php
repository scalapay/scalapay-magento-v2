<?php

namespace Scalapay\Scalapay\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();


        if ($connection->tableColumnExists($installer->getTable('sales_order'), 'order_token') === false) {
            $connection
                ->addColumn(
                    $setup->getTable($installer->getTable('sales_order')),
                    'order_token',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Scalapay Order token',
                        'nullable' => true
                    ]
                );
        }

        $installer->endSetup();
    }
}
