<?php

namespace Scalapay\Scalapay\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();
        if ($connection->tableColumnExists($installer->getTable('sales_order'), 'order_token') === false) {
            $connection
                ->addColumn(
                    $setup->getTable($installer->getTable('sales_order')),
                    'order_token',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 50,
                        'comment' => 'Scalapay Order token',
                        'nullable' => true
                    ]
                );
            $setup->endSetup();
        }
    }
}
