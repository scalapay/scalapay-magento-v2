<?php

namespace Scalapay\Scalapay\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Message\ManagerInterface;
use Scalapay\Scalapay\Helper\Data as HelperData;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Event\Observer;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Checkout\Model\Session;

class PreventOrder implements ObserverInterface
{
    protected $_helper;
    protected $_url;
    protected $_messageManager;
    protected $_responseFactory;
    protected $_orderManagement;
    protected $_orderFactory;
    protected $_checkoutSession;

    public function __construct(
        HelperData $helperData,
        UrlInterface $url,
        ManagerInterface $messageManager,
        ResponseFactory $responseFactory,
        OrderManagementInterface $orderManagement,
        OrderFactory $orderFactory,
        Session $checkoutSession
    ) {
        $this->_helper = $helperData;
        $this->_url = $url;
        $this->_messageManager = $messageManager;
        $this->_responseFactory = $responseFactory;
        $this->_orderManagement = $orderManagement;
        $this->_orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;
    }

    public function execute(Observer $observer)
    {
        $isNotDirect = $this->_checkoutSession->getData('scalapay_order_place_before');
        $event = $observer->getEvent();
        $orderObj = $event->getOrder();
        if ($orderObj) {
            $order_id = $orderObj->getEntityId();
            if ($order_id) {
                $order = $this->_orderFactory->create()->load($order_id);
                $payment = $order->getPayment();
                $payment_method_code = $payment->getMethodInstance()->getCode();
                if ($payment_method_code == 'scalapay') {
                    if ($isNotDirect != 'proper') {
                        $error_message =  __('An error occurred. Please try again.');
                        $this->_messageManager->addError($error_message);
                        $redirectionUrl = $this->_url->getUrl('checkout/cart');
                        if ($order_id) {
                            $this->_helper->log("canceling order id: " . $order_id);
                            $this->_orderManagement->cancel($order_id);
                            $this->_checkoutSession->setData('scalapay_order_place_before', '');

                            try {
                                $this->_responseFactory->create()->setRedirect($redirectionUrl)->sendResponse();
                            } catch (\Exception $e) {
                                $this->_helper->log($e->getMessage());
                                header("Location: " . $redirectionUrl);
                            }
                            exit;
                        }
                    }
                }
            }
        }

        return $this;
    }
}
