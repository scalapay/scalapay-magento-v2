<?php

namespace Scalapay\Scalapay\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Scalapay\Scalapay\Helper\Data as HelperData;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;

class SavePaymentConfig implements ObserverInterface
{
    protected $configWriter;
    protected $helper;
    protected $cacheTypeList;
    protected $cacheFrontendPool;
    protected $messageManager;

    public function __construct(
        HelperData $helper,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool
    ) {
        $this->helper = $helper;
        $this->configWriter = $configWriter;
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
        $this->messageManager = $messageManager;
    }

    public function execute(EventObserver $observer)
    {
        $this->savePaymentSection();
        return $this;
    }

    /**
     * Advanced save procedure
     *
     * @return bool
     */
    protected function savePaymentSection()
    {
        $this->helper->log("savePaymentSection payment config after save.");
        $config = $this->helper->getScalapayConfigV3();
        $message = [];
        $isError = false;

        if (!isset($config['payin3']["maximumAmount"]) && $this->helper->getConfigData('active')) {
            $this->messageManager->addError(__("Scalapay - Pay in 3 is not available."));
            $this->configWriter->save('payment/scalapay/active', 0);
        }
        if (!isset($config['payin4']["maximumAmount"]) && $this->helper->getConfigDataPayin4('active')) {
            $this->messageManager->addError(__("Scalapay - Pay in 4 is not available."));
            $this->configWriter->save('payment/payin4/active', 0);
        }
        if (!isset($config['paylater']["maximumAmount"]) && $this->helper->getConfigDataPayLater('active')) {
            $this->messageManager->addError(__("Scalapay - Pay later is not available."));
            $this->configWriter->save('payment/paylater/active', 0);
        }

        // diabling the payment methods:
        $this->cleanCacheByScalapay();

        return $isError;
    }

    public function cleanCacheByScalapay()
    {
        $_types = [
            'config',
            'full_page'
        ];
        foreach ($_types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
