<?php

namespace Scalapay\Scalapay\Helper;

use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    public $_checkoutSession;
    protected $_ws_url;
    protected $_ws_password;
    protected $_urlInterface;
    protected $_scopeConfig;
    protected $_jsonHelper;
    protected $_shippingMethod;
    protected $_pageFactory;
    protected $_storeResolver;
    protected $_storeManager;
    protected $_datetime;
    protected $_scalapayConfigv3;
    protected $_scalapayConfig;
    protected $_scalapayPayin4Config;
    protected $_scalapayPayLaterConfig;
    protected $_priceHelper;
    protected $_store;
    protected $_productFactory;
    protected $_productRepository;
    protected $_categoryRepository;
    protected $_directory;
    protected $_cart;
    protected $_moduleList;
    protected $_productMetadataInterface;
    protected $_regionFactory;
    protected $_scalapayConfigV3;
    protected $_productRepositoryInterfaceFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Api\Data\ShippingMethodInterface $shippingMethod,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Locale\Resolver $storeResolver,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Framework\Locale\Resolver $store,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Directory\Helper\Data $directory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Module\ModuleList $moduleList,
        \Magento\Framework\App\ProductMetadataInterface $productMetadataInterface,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory
    ) {
        $this->_curl = $curl;
        $this->_urlInterface = $urlInterface;
        $this->_scopeConfig = $scopeConfig;
        $this->_jsonHelper = $jsonHelper;
        $this->_shippingMethod = $shippingMethod;
        $this->_checkoutSession = $checkoutSession;
        $this->_pageFactory = $pageFactory;
        $this->_storeResolver = $storeResolver;
        $this->_storeManager = $storeManager;
        $this->_datetime = $datetime;
        $this->_priceHelper = $priceHelper;
        $this->_store = $store;
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_categoryRepository = $categoryRepository;
        $this->_directory = $directory;
        $this->_cart = $cart;
        $this->_moduleList = $moduleList;
        $this->_productMetadataInterface = $productMetadataInterface;
        $this->_regionFactory = $regionFactory;
        $this->_productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;

        $mode = $this->getConfigData("mode");

        if ($mode == 0) {
            $this->_ws_url = $this->getConfigData("test_url");
            $this->_ws_password = $this->getConfigData("test_api_password");
        } else {
            $this->_ws_url = $this->getConfigData("production_url");
            $this->_ws_password = $this->getConfigData("production_api_password");
        }

        parent::__construct($context);
    }

    /**
     * Read response for Scalapay curl request
     *
     * @return Array
     *
     */
    public function getScalapayConfig()
    {
        if (!isset($this->_scalapayConfig)) {
            $this->_scalapayConfig = [
                "minimumAmount" => $this->getConfigData('min_amount'),
                "maximumAmount" => $this->getConfigData('max_amount'),
                "numberOfPayments" => 3,
                "promotionUrl" => "https://promotion.scalapay.com/popup/default/",
                "locales" => ["it","fr","en"]
            ];
        }

        return $this->_scalapayConfig;
    }

    /**
     * Read response for Scalapay curl request
     *
     * @return Array
     *
     */
    public function getScalapayPayin4Config()
    {
        if (!isset($this->_scalapayPayin4Config)) {
            $this->_scalapayPayin4Config = [
                "minimumAmount" => $this->getConfigDataPayin4('min_amount'),
                "maximumAmount" => $this->getConfigDataPayin4('max_amount'),
                "numberOfPayments" => 4,
                "promotionUrl" => "https://promotion.scalapay.com/popup/default/",
                "locales" => ["it","fr","en"]
            ];
        }

        return $this->_scalapayPayin4Config;
    }
    /**
    * Read response for Scalapay curl request
    *
    * @return Array
    *
    */
    public function getScalapayPayLaterConfig()
    {
        if (!isset($this->_scalapayPayLaterConfig)) {
            $this->_scalapayPayLaterConfig = [
                "minimumAmount" => $this->getConfigDataPayLater('min_amount'),
                "maximumAmount" => $this->getConfigDataPayLater('max_amount'),
                "numberOfPayments" => 1,
                "promotionUrl" => "https://promotion.scalapay.com/popup/default/",
                "locales" => ["it","fr","en"]
            ];
        }

        return $this->_scalapayPayLaterConfig;
    }

    /**
     * get Scalapay V3 configurations
     *
     * @return Array
     *
     */
    public function getScalapayConfigV3()
    {
        $this->log("calling get Scalapay config V3.");
        if (!isset($this->_scalapayConfigV3)) {
            $this->log("getting Scalapay config V3 values.");
            $urlKey = "v3/configurations";
            $response = $this->getCurlHandler($urlKey);
            if ($response != "Unauthorized") {
                $ins = $this->_jsonHelper->jsonDecode($response);
                if (isset($ins)) {
                    foreach ($ins as $res) {
                        if ($res["product"] == 'pay-in-3' && $res["type"] == 'online') {
                            if (isset($res["configuration"]["minimumAmount"]["amount"])) {
                                $this->_scalapayConfigV3['payin3']["minimumAmount"] = $res["configuration"]["minimumAmount"]["amount"];
                            }
                            if (isset($res["configuration"]["maximumAmount"]["amount"])) {
                                $this->_scalapayConfigV3['payin3']["maximumAmount"] = $res["configuration"]["maximumAmount"]["amount"];
                            }
                            $this->_scalapayConfigV3['payin3']["numberOfPayments"] = 3;
                        }
                        if ($res["product"] == 'pay-in-4' && $res["type"] == 'online') {
                            if (isset($res["configuration"]["minimumAmount"]["amount"])) {
                                $this->_scalapayConfigV3['payin4']["minimumAmount"] = $res["configuration"]["minimumAmount"]["amount"];
                            }
                            if (isset($res["configuration"]["maximumAmount"]["amount"])) {
                                $this->_scalapayConfigV3['payin4']["maximumAmount"] = $res["configuration"]["maximumAmount"]["amount"];
                            }
                            $this->_scalapayConfigV3['payin4']["numberOfPayments"] = 4;
                        }
                        if ($res["product"] == 'later' && $res["type"] == 'online') {
                            if (isset($res["configuration"]["minimumAmount"]["amount"])) {
                                $this->_scalapayConfigV3['paylater']["minimumAmount"] = $res["configuration"]["minimumAmount"]["amount"];
                            }
                            if (isset($res["configuration"]["maximumAmount"]["amount"])) {
                                $this->_scalapayConfigV3['paylater']["maximumAmount"] = $res["configuration"]["maximumAmount"]["amount"];
                            }
                            $this->_scalapayConfigV3['paylater']["numberOfPayments"] = 1;
                            $this->_scalapayConfigV3['paylater']["number"] = $res["frequency"]["number"];
                        }
                    }
                }
            }
        }

        return $this->_scalapayConfigV3;
    }

    /**
     * Return promotion url on the base of language
     *
     * @return string
     */
    public function getPromotionUrl()
    {
        $configData = $this->getScalapayConfig();
        $allowedLanguages = $configData["locales"];
        $locales = $this->_store->getLocale();
        $promotionUrl = $configData["promotionUrl"];
        if ($locales != null) {
            $language = substr($locales, 0, 2);
            if ($configData["promotionUrl"] != "") {
                if (in_array($language, $allowedLanguages)) {
                    $promotionUrl = $configData["promotionUrl"] . $language . "/";
                }
            }
        }

        return $promotionUrl;
    }

    /**
     * Return current language code
     *
     * @return string
     */
    public function getCurrentLanguage()
    {
        $locale = $this->_store->getLocale();
        $language = substr($locale, 0, 2);

        return $language;
    }

    /**
    * Send  merchent refference to  Scalapay after order success
    *
    * @param string $urlKey
    * @param array|null $data
    *
    * @return array
    */

    protected function getCurlHandler($urlKey, $data = "")
    {
        $response = "";

        if ($urlKey != "") {
            try {
                $url = $this->_ws_url . "/" . $urlKey;
                $curl = curl_init();
                $curlOption = array(
                  CURLOPT_URL => $url,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Authorization: Bearer ' . $this->_ws_password,
                    'Content-Type: application/json'
                  ),
                );
                if (isset($data) && $data != "") {
                    $curlOption[CURLOPT_POSTFIELDS] = $data;
                }
                curl_setopt_array($curl, $curlOption);
                $response = curl_exec($curl);
                $this->log($response);
                curl_close($curl);
            } catch (\Exception $e) {
                $response = $this->_jsonHelper->jsonEncode(['error' => $e->getMessage()]);
                $this->log($e->getMessage());
            }
        }

        return $response;
    }

    /**
     * Get Place Scalapay Order
     *
     * @return Array
     *
     */
    public function scalapayOrder($params)
    {
        $json_array = [];
        $this->log("Scalapay Order: ");
        $this->log("Params: ");
        $this->log($params);
        $quote = $this->getQuote();
        //selected payment method
        $paymentMethod = $quote->getPayment()->getMethod();
        $DiscountTotal = 0;
        $currency = $quote->getQuoteCurrencyCode();

        //get cartbilling and shipping addresses
        $shipping = $quote->getShippingAddress();
        $billing = $quote->getBillingAddress();

        $this->log("Billing data: ");
        $this->log($billing->getData());
        $this->log("Shipping data: ");
        $this->log($shipping->getData());
        $this->log("cehckout method type: " . $quote->getCheckoutMethod());

        if (
            $quote->getCheckoutMethod() === \Magento\Checkout\Model\Type\Onepage::METHOD_GUEST ||
            $quote->getCheckoutMethod() === null
        ) {
            $this->log("Customer Type - Guest: " . \Magento\Checkout\Model\Type\Onepage::METHOD_GUEST);
        //do nothing
        } elseif (
            $quote->getCheckoutMethod() === \Magento\Checkout\Model\Type\Onepage::METHOD_REGISTER
        ) {
            $this->log(
                "Customer Type - new registered customer: " . \Magento\Checkout\Model\Type\Onepage::METHOD_REGISTER
            );
        //do nothing
        } else {
            $this->log(
                "Customer Type - registered customer: " . \Magento\Checkout\Model\Type\Onepage::METHOD_REGISTER
            );
            //checks for registered customer - validate customer data
            $validationErrors = [];
            $billing->setStoreId($quote->getStoreId());
            $validationResult = $billing->validate();
            if ($validationResult !== true) {
                foreach ($validationResult as $billValObj) {
                    $errors = __("Billing") . " " . $billValObj->__toString() . "<br />\n";
                    $validationErrors = array_merge($validationErrors, [$errors]);
                }
            }

            if (!$quote->isVirtual()) {
                $shipping->setStoreId($quote->getStoreId());
                $validationResultShipping = $shipping->validate();
                if ($validationResultShipping !== true) {
                    foreach ($validationResultShipping as $shippValObj) {
                        $errors = __("Shipping") . " " . $shippValObj->__toString() . "<br />\n";
                        $validationErrors = array_merge($validationErrors, [$errors]);
                    }
                }
            }

            if (count($validationErrors) > 0) {
                $this->log("Address validation result: ");
                $message = implode("\n", $validationErrors);
                $this->log($message);
                $return_error_arrays["error"] = 1;
                $return_error_arrays["message"] = $message;
                return $return_error_arrays;
            }
        }

        //Billing information
        $city = $this->ctrim($billing->getCity());
        if ($city == "" && isset($params['billing']['city'])) {
            $city = $this->ctrim($params['billing']['city']);
        }
        if ($city == "") {
            $city = $this->ctrim($shipping->getCity());
        }
        if ($city != "") {
            $billing->setCity($city);
        }

        $post_code = $this->ctrim($billing->getPostcode());
        if ($post_code == "" && isset($params['billing']['postcode'])) {
            $post_code = $this->ctrim($params['billing']['postcode']);
        }
        if ($post_code == "") {
            $post_code = $this->ctrim($shipping->getPostcode());
        }
        if ($post_code != "") {
            $billing->setPostcode($post_code);
        }

        $email = $this->ctrim($billing->getEmail());
        if ($email == "") {
            $email = ($quote->getCustomerEmail());
        }
        if ($email == "" && isset($params['billing']['email'])) {
            $email = $this->ctrim($params['billing']['email']);
        }
        if ($email == "" && isset($params["email"])) {
            $email = $this->ctrim($params["email"]);
        }
        if ($email == "") {
            $email = $this->ctrim($shipping->getEmail());
        }
        if ($email != "") {
            $billing->setEmail($email);
        }

        $telephone = $this->ctrim($billing->getTelephone());
        if ($telephone == "" && isset($params['billing']['telephone'])) {
            $telephone = $this->ctrim($params['billing']['telephone']);
        }
        if ($telephone == "") {
            $telephone = $this->ctrim($shipping->getTelephone());
        }
        if ($telephone != "") {
            $billing->setTelephone($telephone);
        }

        $state = empty($billing->getRegion()) ? '' : $this->ctrim($billing->getRegion());
        if ($state == "" && isset($params['billing']['region'])) {
            $state = $this->ctrim($params['billing']['region']);
        }
        if ($state == "") {
            $state = empty($shipping->getRegion()) ? '' : $this->ctrim($shipping->getRegion());
        }
        if ($state != "") {
            $billing->setRegion($state);
        }

        $country_code = $this->ctrim($billing->getCountryId());
        if ($country_code == "" && isset($params['billing']['country_id'])) {
            $country_code = $this->ctrim($params['billing']['country_id']);
        }
        if ($country_code == "") {
            $country_code = $this->ctrim($shipping->getCountryId());
        }
        if ($country_code != "") {
            $billing->setCountryId($country_code);
        }

        $region_id = empty($billing->getRegionId()) ? '' : $this->ctrim($billing->getRegionId());
        if ($region_id == "" && isset($params['billing']['region_id'])) {
            $region_id = $this->ctrim($params['billing']['region_id']);
        }
        if ($region_id == "") {
            $region_id = empty($shipping->getRegionId()) ? '' : $this->ctrim($shipping->getRegionId());
        }

        if ($region_id != "") {
            $this->log("Billing Region id case:");
            // call here region code
            $region_id = $this->processRegionId($region_id, $state, $country_code);
            if (isset($region_id) && $region_id != "") {
                $billing->setRegionId($region_id);
            }
        }

        $street  = $billing->getStreet();
        $sstreet = $shipping->getStreet();
        $street_address0 = $this->ctrim($street[0]);
        if ($street_address0 == "" && isset($params['billing']['street'][0])) {
            $street_address0 = $this->ctrim($params['billing']['street'][0]);
        }
        if ($street_address0 == "") {
            $street_address0 = $this->ctrim($sstreet[0]);
        }
        $street_address1 = "";
        if (isset($street[1])) {
            $street_address1 = $this->ctrim($street[1]);
            if ($street_address1 == "" && isset($params['billing']['street'][1])) {
                $street_address1 = $this->ctrim($params['billing']['street'][1]);
            }
            if ($street_address1 == "") {
                $street_address1 = $this->ctrim($sstreet[1]);
            }
        }
        $street_address2 = "";
        if (isset($street[2])) {
            $street_address2 = $this->ctrim($street[2]);
            if ($street_address2 == "" && isset($params['billing']['street'][2])) {
                $street_address2 = $this->ctrim($params['billing']['street'][2]);
            }
            if ($street_address2 == "") {
                $street_address2 = $this->ctrim($sstreet[2]);
            }
        }
        $billingStreetAddress = array();
        if (isset($street_address0)) {
            $billingStreetAddress[0] = $street_address0;
            $billingStreetAddress[1] = "";
            $billingStreetAddress[2] = "";
        }

        if (isset($street_address1)) {
            $billingStreetAddress[1] = $street_address1;
            $billingStreetAddress[2] = "";
        }

        if (isset($street_address2)) {
            $billingStreetAddress[2] = $street_address2;
        }
        $billing->setStreet($billingStreetAddress);

        $firstname = $this->ctrim($billing->getFirstname());
        if ($firstname == "") {
            $firstname = $this->ctrim($quote->getCustomerFirstname());
        }
        if ($firstname == "" && isset($params['billing']['firstname'])) {
            $firstname = $this->ctrim($params['billing']['firstname']);
        }
        if ($firstname == "") {
            $firstname = $this->ctrim($shipping->getFirstname());
        }
        if ($firstname != "") {
            $billing->setFirstname($firstname);
        }

        $last_name = $this->ctrim($billing->getLastname());
        if ($last_name == "") {
            $last_name = $this->ctrim($quote->getCustomerLastname());
        }
        if ($last_name == "" && isset($params['billing']['lastname'])) {
            $last_name = $this->ctrim($params['billing']['lastname']);
        }
        if ($last_name == "") {
            $last_name = $this->ctrim($shipping->getLastname());
        }
        if ($last_name != "") {
            $billing->setLastname($last_name);
        }

        $full_name = $firstname . " " . $last_name;

        $billing->save();

        $use_for_shipping = 0;
        if (isset($params['billing']['use_for_shipping'])) {
            $use_for_shipping = $this->ctrim($params['billing']['use_for_shipping']);
        }
        if (
            $use_for_shipping == 1 ||
            $billing->getData('same_as_billing') == 1 || $shipping->getData('same_as_billing') == 1
        ) {
            $shipping->setCity($city);
            $city_shipping = $this->ctrim($city);
            $shipping->setPostcode($post_code);
            $post_code_shipping = $this->ctrim($post_code);
            $email_shipping = "";
            if ($email != "") {
                $shipping->setEmail($email);
                $email_shipping = $this->ctrim($email);
            }

            $shipping->setTelephone($telephone);
            $telephone_shipping = $this->ctrim($telephone);
            $shipping->setRegion($state);
            $state_shipping = $this->ctrim($state);
            $shipping->setRegionId($region_id);
            $region_id_shipping = $this->ctrim($region_id);
            $shippingStreetAddress = array();
            if (isset($street_address0)) {
                $shippingStreetAddress[0] = $street_address0;
                $shippingStreetAddress[1] = "";
                $shippingStreetAddress[2] = "";
            }

            if (isset($street_address1)) {
                $shippingStreetAddress[1] = $street_address1;
                $shippingStreetAddress[2] = "";
            }

            if (isset($street_address2)) {
                $shippingStreetAddress[2] = $street_address2;
            }
            $shipping->setStreet($shippingStreetAddress);
            $street_address_shipping0 = $this->ctrim($street_address0);
            $shipping->setFirstname($firstname);
            $firstname_shipping = $this->ctrim($firstname);
            $shipping->setLastname($last_name);
            $last_name_shipping = $this->ctrim($last_name);
            $shipping->setCountryId($country_code);
            $country_code_shipping = $this->ctrim($country_code);
        } else {
            //Shipping information
            $city_shipping = $this->ctrim($shipping->getCity());
            if ($city_shipping == "" && isset($params['shipping']['city'])) {
                $city_shipping = $this->ctrim($params['shipping']['city']);
            }
            $shipping->setCity($city_shipping);

            $post_code_shipping = $this->ctrim($shipping->getPostcode());
            if ($post_code_shipping == "" && isset($params['shipping']['postcode'])) {
                $post_code_shipping = $this->ctrim($params['shipping']['postcode']);
            }
            $shipping->setPostcode($post_code_shipping);
            $email_shipping = $this->ctrim($shipping->getEmail());
            if ($email_shipping == "") {
                $email_shipping = $this->ctrim($quote->getCustomerEmail());
            }
            if ($email_shipping == "" && isset($params['billing']['email'])) {
                $email_shipping = $this->ctrim($params['billing']['email']);
            }
            if ($email_shipping != "") {
                $shipping->setEmail($email_shipping);
            }
            $telephone_shipping = $this->ctrim($shipping->getTelephone());
            if ($telephone_shipping == "" && isset($params['shipping']['telephone'])) {
                $telephone_shipping = $this->ctrim($params['shipping']['telephone']);
            }
            $shipping->setTelephone($telephone_shipping);

            $country_code_shipping = $this->ctrim($shipping->getCountryId());
            if ($country_code_shipping == "" && isset($params['shipping']['country_id'])) {
                $country_code_shipping = $this->ctrim($params['shipping']['country_id']);
            }
            $shipping->setCountryId($country_code_shipping);

            $state_shipping = empty($shipping->getRegion()) ? '' : $this->ctrim($shipping->getRegion());
            if ($state_shipping == "" && isset($params['shipping']['region'])) {
                $state_shipping = $this->ctrim($params['shipping']['region']);
            }
            $shipping->setRegion($state_shipping);

            $region_id_shipping = empty($shipping->getRegionId()) ? '' : $this->ctrim($shipping->getRegionId());
            if ($region_id_shipping == "" && isset($params['shipping']['region_id'])) {
                $region_id_shipping = $this->ctrim($params['shipping']['region_id']);
            }

            if ($region_id_shipping != "") {
                $this->log("Shipping Region id case:");
                // call here region code
                $region_id_shipping = $this->processRegionId(
                    $region_id_shipping,
                    $state_shipping,
                    $country_code_shipping
                );
                if ($region_id_shipping != "") {
                    $shipping->setRegionId($region_id_shipping);
                }
            }

            $street_shipping = $shipping->getStreet();
            $street_address_shipping0 = $this->ctrim($street_shipping[0]);
            if ($street_address_shipping0 == "" && isset($params['shipping']['street'][0])) {
                $street_address_shipping0 = $this->ctrim($params['shipping']['street'][0]);
            }
            $street_address_shipping1 = "";
            if (isset($street_shipping[1])) {
                $street_address_shipping1 = $this->ctrim($street_shipping[1]);
                if ($street_address_shipping1 == "" && isset($params['shipping']['street'][1])) {
                    $street_address_shipping1 = $this->ctrim($params['shipping']['street'][1]);
                }
            }
            $street_address_shipping2 = "";
            if (isset($street_shipping[2])) {
                $street_address_shipping2 = $this->ctrim($street_shipping[2]);
                if ($street_address_shipping2 == "" && isset($params['shipping']['street'][2])) {
                    $street_address_shipping2 = $this->ctrim($params['shipping']['street'][2]);
                }
            }
            $shippingStreetAddress = array();
            if (isset($street_address_shipping0)) {
                $shippingStreetAddress[0] = $street_address_shipping0;
                $shippingStreetAddress[1] = "";
                $shippingStreetAddress[2] = "";
            }

            if (isset($street_address_shipping1)) {
                $shippingStreetAddress[1] = $street_address_shipping1;
                $shippingStreetAddress[2] = "";
            }

            if (isset($street_address_shipping2)) {
                $shippingStreetAddress[2] = $street_address_shipping2;
            }
            $shipping->setStreet($shippingStreetAddress);
            $firstname_shipping = $this->ctrim($shipping->getFirstname());
            if ($firstname_shipping == "") {
                $firstname_shipping = $this->ctrim($quote->getCustomerFirstname());
            }
            if ($firstname_shipping == "" && isset($params['shipping']['firstname'])) {
                $firstname_shipping = $this->ctrim($params['shipping']['firstname']);
            }
            $shipping->setFirstname($firstname_shipping);

            $last_name_shipping = $this->ctrim($shipping->getLastname());
            if ($last_name_shipping == "") {
                $last_name_shipping = $this->ctrim($quote->getCustomerLastname());
            }
            if ($last_name_shipping == "" && isset($params['shipping']['lastname'])) {
                $last_name_shipping = $this->ctrim($params['shipping']['lastname']);
            }
            $shipping->setLastname($last_name_shipping);
        }

        $shipping->save();
        if ($quote->getCustomerEmail() === null && $billing->getEmail() !== null) {
            $quote->setCustomerEmail($billing->getEmail());
            $quote->save();
        }
        /// recalculate total
        $quote->collectTotals()->save();
        //get cart subtotal and grand total
        $grandTotal = $quote->getGrandTotal();
        $full_name_shipping = $firstname_shipping . " " . $last_name_shipping;
        $merchantReference = $quote->getId();

        $requiredFields = array(
            'consumer_givenNames' => '',
            'consumer_surname' => '',
            'consumer_email' => '',
            'consumer_phoneNumber' => '',
            'billing_name' =>  '',
            'billing_line1' =>  '',
            'billing_suburb' =>  '',
            'billing_state' =>  '',
            'billing_state_id' =>  '',
            'billing_postcode' =>  '',
            'billing_countryCode' =>  '',
            'billing_phoneNumber' =>  '',
            'shipping_name' =>  '',
            'shipping_line1' =>  '',
            'shipping_suburb' =>  '',
            'shipping_state' =>  '',
            'shipping_state_id' =>  '',
            'shipping_postcode' =>  '',
            'shipping_countryCode' =>  '',
            'shipping_phoneNumber' =>  ''
          );

        $json_array["totalAmount"]["amount"] = "$grandTotal";
        $json_array["totalAmount"]["currency"] = "$currency";
        if (isset($telephone) && $telephone != "") {
            $json_array["consumer"]["phoneNumber"] = $telephone;
            $requiredFields['consumer_phoneNumber'] = $telephone;
        }
        $json_array["consumer"]["givenNames"] = $firstname;
        $requiredFields['consumer_givenNames'] = $firstname;
        $json_array["consumer"]["surname"] = $last_name;
        $requiredFields['consumer_surname'] = $last_name;
        $json_array["consumer"]["email"] = $email;
        $requiredFields['consumer_email'] = $email;
        //billing info

        $json_array["billing"]["name"] = $full_name;
        $requiredFields['billing_name'] = $full_name;
        if (isset($street_address0) && $street_address0 != "") {
            $json_array["billing"]["line1"] = $street_address0;
            $requiredFields['billing_line1'] = $street_address0;
        }
        if (isset($city) && $city != "") {
            $json_array["billing"]["suburb"] = $city;
            $requiredFields['billing_suburb'] = $city;
        }
        if (isset($state) && $state != "") {
            $json_array["billing"]["state"] = $state;
            $requiredFields['billing_state'] = $state;
        }
        if (isset($region_id) && $region_id != "") {
            $requiredFields['billing_state_id'] = $region_id;
        }
        if (isset($post_code) && $post_code != "") {
            $json_array["billing"]["postcode"] = $post_code;
            $requiredFields['billing_postcode'] = $post_code;
        }
        if (isset($country_code) && $country_code != "") {
            $json_array["billing"]["countryCode"] = $country_code;
            $requiredFields['billing_countryCode'] = $country_code;
        }
        if (isset($telephone) && $telephone != "") {
            $json_array["billing"]["phoneNumber"] = $telephone;
            $requiredFields['billing_phoneNumber'] = $telephone;
        }
        //shipping info
        $json_array["shipping"]["name"] = $full_name_shipping;
        $requiredFields['shipping_name'] = $full_name_shipping;
        if (isset($street_address_shipping0) && $street_address_shipping0 != "") {
            $json_array["shipping"]["line1"] = $street_address_shipping0;
            $requiredFields['shipping_line1'] = $street_address_shipping0;
        }
        if (isset($city_shipping) && $city_shipping != "") {
            $json_array["shipping"]["suburb"] = $city_shipping;
            $requiredFields['shipping_suburb'] = $city_shipping;
        }
        if (isset($state_shipping) && $state_shipping != "") {
            $json_array["shipping"]["state"] = $state_shipping;
            $requiredFields['shipping_state'] = $state_shipping;
        }
        if (isset($region_id_shipping) && $region_id_shipping != "") {
            $requiredFields['shipping_state_id'] = $region_id_shipping;
        }
        if (isset($post_code_shipping) && $post_code_shipping != "") {
            $json_array["shipping"]["postcode"] = $post_code_shipping;
            $requiredFields['shipping_postcode'] = $post_code_shipping;
        }
        if (isset($country_code_shipping) && $country_code_shipping != "") {
            $json_array["shipping"]["countryCode"] = $country_code_shipping;
            $requiredFields['shipping_countryCode'] = $country_code_shipping;
        }
        if (isset($telephone_shipping) && $telephone_shipping != "") {
            $json_array["shipping"]["phoneNumber"] = $telephone_shipping;
            $requiredFields['shipping_phoneNumber'] = $telephone_shipping;
        }
        $items_json = "";
        $productIds = [];
        $getAllitems = $quote->getAllItems();
        foreach ($getAllitems as $item) {
            if (
                 $item->getProductType() == 'configurable' ||
                 $item->getProductType() == 'grouped' ||
                 $item->getProductType() == 'bundle'
            ) {
                // do nothing
            } else {
                $productIds[] = $item->getProductId();
            }
            if ($item->getParentItemId()) {
                continue;
            }

            $scalapay_item = [];
            $name = $item->getName();
            $sku = $item->getSku();
            $qty = $item->getQty();

            if ($qty == "") {
                $qty = 1;
            }

            try {
                $_product = $this->_productRepository->get($sku);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $_product = $this->_productRepository->getById($item->getProductId());
                $this->log($e->getMessage());
            }

            if ($_product) {
                $this->log("Product id: " . $_product->getId());
                $categoryIds = $_product->getCategoryIds();
                $this->log("category ids: ");
                $this->log($categoryIds);

                $categoryName = "";
                $subCategory  = array();
                $gtin = "";

                if (count($categoryIds) > 0) {
                    foreach ($categoryIds as $c) {
                        if ($c > 2) {
                            try {
                                $category = $this->_categoryRepository->get(
                                    $c,
                                    $this->_storeManager->getStore()->getId()
                                );
                            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                                $this->log($e->getMessage());
                                continue;
                            }

                            if ($category) {
                                if ($category->getLevel() == 2) {
                                    $categoryName = $category->getName();
                                } elseif ($category->getLevel() >= 3) {
                                    $subCategory[] = $category->getName();
                                }
                            }
                        }
                    }
                }

                $scalapay_item["name"] = $name;
                if ($categoryName != "") {
                    $this->log("category name: ");
                    $this->log($categoryName);
                    $scalapay_item["category"] = $categoryName; // "clothes"
                }
                if (count($subCategory) > 0) {
                    $this->log("subCategory: ");
                    $this->log($subCategory);
                    $scalapay_item["subcategory"] = $subCategory; // ["shirt", "long-sleeve"]
                }

                $brand = $_product->getResource()->getAttribute('brand');

                if ($brand) {
                    $brand = $_product->getAttributeText('brand');

                    $this->log("brand1: ");
                    $this->log($brand);
                } else {
                    $brand = $_product->getAttributeText('manufacturer');

                    $this->log("manufacturer: ");
                    $this->log($brand);
                }

                if ($brand) {
                    $scalapay_item["brand"] = $brand; // "TopChoice"
                }
                if ($gtin != "") {
                    $scalapay_item["gtin"] = $gtin; // "123458791330"
                }
            }

            $scalapay_item["name"] = $name;
            $scalapay_item["sku"] = $sku;
            $scalapay_item["quantity"] = $qty;
            $item_price = $item->getPrice();
            $scalapay_item["price"]["amount"] = "$item_price";
            $scalapay_item["price"]["currency"] = "$currency";
            $scalapay_item["pageUrl"] = $_product->getProductUrl();
            $scalapay_item["imageUrl"] = $this->getProductThumbnailUrl($_product->getId());
            $json_array["items"][] = $scalapay_item;
            $item_price = $item->getPrice();
            $DiscountTotal += $item->getDiscountAmount();
        }
        $payment = $quote->getPayment();
        if ($paymentMethod == "payin4") {
            if ($payment && $payment->getMethod() == "") {
                $this->log("data process Action : set payin4 payment method.");
                $payment->setMethod("payin4");
                $payment->save();
            }
            if (!$this->isScalapayPayin4Active($grandTotal, $productIds, $country_code_shipping)) {
                $return_error_arrays = [];
                $return_error_arrays["error"] = 1;
                $return_error_arrays["message"] = __("The payment was not successful. Choose another payment method");
                $this->log("is payin4 error:");
                $this->log($return_error_arrays);
                return $return_error_arrays;
            }
        } elseif ($paymentMethod == "paylater") {
            if ($payment && $payment->getMethod() == "") {
                $this->log("data process Action : set paylater payment method.");
                $payment->setMethod("paylater");
                $payment->save();
            }
            if (!$this->isScalapayPayLaterActive($grandTotal, $productIds, $country_code_shipping)) {
                $return_error_arrays = [];
                $return_error_arrays["error"] = 1;
                $return_error_arrays["message"] = __("The payment was not successful. Choose another payment method");
                $this->log("is paylater error:");
                $this->log($return_error_arrays);
                return $return_error_arrays;
            }
        } else {
            if ($payment && $payment->getMethod() == "") {
                $this->log("data process Action : set scalapay payment method.");
                $payment->setMethod("scalapay");
                $payment->save();
            }
            if (!$this->isScalapayActive($grandTotal, $productIds, $country_code_shipping)) {
                $return_error_arrays = [];
                $return_error_arrays["error"] = 1;
                $return_error_arrays["message"] = __("The payment was not successful. Choose another payment method");
                $this->log("is isScalapayActive error:");
                $this->log($return_error_arrays);
                return $return_error_arrays;
            }
        }



        $isAddressProblem = false;
        $skippedFields = array();

        if (!$this->_directory->isRegionRequired($country_code_shipping)) {
            array_push($skippedFields, 'shipping_state');
            array_push($skippedFields, 'shipping_state_id');
        } else {
            $shippingRegions = $this->_directory->getRegionData();
            if (array_key_exists($country_code_shipping, $shippingRegions)) {
                array_push($skippedFields, 'shipping_state');
            } else {
                array_push($skippedFields, 'shipping_state_id');
            }
        }
        if (!$this->_directory->isRegionRequired($country_code)) {
            array_push($skippedFields, 'billing_state');
            array_push($skippedFields, 'billing_state_id');
        } else {
            $billingRegions = $this->_directory->getRegionData();

            if (array_key_exists($country_code, $billingRegions)) {
                array_push($skippedFields, 'billing_state');
            } else {
                array_push($skippedFields, 'billing_state_id');
            }
        }
        if ($quote->isVirtual()) {
            array_push($skippedFields, 'shipping_name');
            array_push($skippedFields, 'shipping_line1');
            array_push($skippedFields, 'shipping_suburb');
            array_push($skippedFields, 'shipping_state');
            array_push($skippedFields, 'shipping_state_id');
            array_push($skippedFields, 'shipping_postcode');
            array_push($skippedFields, 'shipping_countryCode');
            array_push($skippedFields, 'shipping_phoneNumber');
        }
        $this->log("Skipped Fields: ");
        $this->log($skippedFields);

        foreach ($requiredFields as $key => $value) {
            $this->log("Checking Address field key:" . $key . " => " . $value);
            if ($value == "" &&  !in_array($key, $skippedFields)) {
                $isAddressProblem = true;
                $this->log("Address field value missing: key:" . $key . " => " . $value);
                break;
            }
        }

        if ($isAddressProblem) {
            $return_error_arrays = [];
            $return_error_arrays["error"] = 1;
            $return_error_arrays["message"] = __("There is a problem with required data to place the order.");
            $this->log("address error:");
            $this->log($return_error_arrays);
            return $return_error_arrays;
        }

        if (abs($quote->getShippingAddress()->getDiscountAmount()) > 0) {
            $DiscountTotal = abs($quote->getShippingAddress()->getDiscountAmount());
        }

        if (isset($DiscountTotal) && $DiscountTotal > 0) {
            $discount_displayName = $quote->getCouponCode();

            if ($discount_displayName == "") {
                $discount_displayName = "Discount";
            }
            $discounts_item = [];
            $discounts_item["displayName"] = "$discount_displayName";
            $discounts_item["amount"]["amount"] = "$DiscountTotal";
            $discounts_item["amount"]["currency"] = "$currency";
            $json_array["discounts"][] = $discounts_item;
        }

        $taxAmount = $quote->getTaxAmount();
        if ($taxAmount == '' || $taxAmount == 0) {
            $taxAmount = $quote->getShippingAddress()->getTaxAmount();
        }

        if ($taxAmount == "") {
            $taxAmount = 0;
        }

        $json_array["taxAmount"]["amount"] = "$taxAmount";
        $json_array["taxAmount"]["currency"] = "$currency";

        $mageVersion = $this->_productMetadataInterface->getVersion();
        $scalapayVersion = $this->_moduleList->getOne("Scalapay_Scalapay");
        $setupVersion = $scalapayVersion['setup_version'];

        $json_array["pluginDetails"]["pluginVersion"] = "$setupVersion";
        $json_array["pluginDetails"]["platformVersion"] = "$mageVersion";
        $json_array["pluginDetails"]["platform"] = "magento";
        $json_array["pluginDetails"]["customized"] = "0";

        if ($paymentMethod == "payin4") {
            $json_array["type"] = "online";
            $json_array["product"] = "pay-in-4";
            $json_array["frequency"]["frequencyType"] = "monthly";
            $json_array["frequency"]["number"] = "1";
        } elseif ($paymentMethod == "paylater") {
            $json_array["type"] = "online";
            $json_array["product"] = "later";
            $json_array["frequency"]["frequencyType"] = "daily";
            $json_array["frequency"]["number"] = $this->getConfigDataPayLater("number_of_days");
        } else {
            $json_array["type"] = "online";
            $json_array["product"] = "pay-in-3";
            $json_array["frequency"]["frequencyType"] = "monthly";
            $json_array["frequency"]["number"] = "1";
        }

        $confirm_url = $this->_urlInterface
                ->getUrl("scalapay/index/confirm") . "?cart_id=" . $merchantReference . "&total=" . $grandTotal . "&paymentmethod=" . $paymentMethod;
        $cancel_url = $this->_urlInterface
            ->getUrl("scalapay/index/cancel");
        $json_array["merchant"]["redirectConfirmUrl"] = "$confirm_url";
        $json_array["merchant"]["redirectCancelUrl"] = "$cancel_url";

        $shippingAmount = $quote->getShippingAmount();

        if ($shippingAmount == "" || $shippingAmount == 0) {
            $shippingAmount = $quote->getShippingAddress()->getShippingAmount();
        }

        if ($shippingAmount == "") {
            $shippingAmount = 0;
        }

        $json_array["shippingAmount"]["amount"] = "$shippingAmount";
        $json_array["shippingAmount"]["currency"] = "$currency";
        $this->log("Data after process: ");
        $this->log($json_array);
        //json encode array to string
        $data = json_encode($json_array);

        $urlKey = 'v2/orders';
        $response = $this->getCurlHandler($urlKey, $data);
        $this->log($response);
        $return_array = [];

        if ($response != "Bad Request" && $response != "Unauthorized") {
            $res = $this->_jsonHelper->jsonDecode($response);
            //if error
            if (isset($res["error"])) {
                $return_array["error"] = 1;
                $errors = "";
                foreach ($res["error"]["errors"] as $error) {
                    if (count($error["messages"]) > 0) {
                        $errors .= implode(". ", $error["messages"]) . ". ";
                    }
                }

                $return_array["message"] = $errors;
            }
            //if approved
            if (isset($res["token"]) && $res["token"] != "") {
                $return_array["error"] = 0;
                $return_array["message"] = $res["checkoutUrl"];
            }

            if (isset($res["errorCode"]) && isset($res["message"])) {
                $return_array["error"] = 1;
                $return_array["message"] = $res["message"];
            }
        } elseif ($response == "Unauthorized") {
            $return_array["error"] = 1;
            $return_array["message"] = "Unauthorized";
        } else {
            $return_array["error"] = 1;
            $return_array["message"] = "Bad Request";
        }

        return $return_array;
    }

    /**
     * Get Magento Quote
     *
     * @return object
     *
     */
    public function getQuote()
    {
        $quote = $this->_cart->getQuote();

        if ($quote) {
            return $quote;
        }
        return false;
    }

    /**
     * Get order shipping info
     *
     * @return object|bool
     *
     */
    public function getShippingInfo()
    {
        $order = $this->getQuote();

        if ($order) {
            $address = $order->getShippingAddress();

            return $address;
        }
        return false;
    }
    /**
     * Get order billing info
     *
     * @return object
     *
     */
    public function getBillingInfo()
    {
        $order = $this->getQuote();
        if ($order) {
            $address = $order->getBillingAddress();

            return $address;
        }
        return false;
    }
    /**
     * Get checkout session
     *
     * @return object
     *
     */
    public function getCheckOutSession()
    {
        return $this->_checkoutSession;
    }
    /**
     * Capture sclalapay payment
     *
     * @return array
     *
     */
    public function scalapayCapture($orderToken, $incrementId)
    {
        $return_array = [];
        $urlKey = 'v2/payments/capture';
        $data = [
            "token" => $orderToken,
            "merchantReference" => $incrementId
            ];
        $data = json_encode($data);
        $response = $this->getCurlHandler($urlKey, $data);
        $res = $this->_jsonHelper->jsonDecode($response);

        //if error
        if (isset($res["errorCode"]) && $res["errorCode"] != "") {
            $return_array["error"] = 1;
            $return_array["message"] = $res["message"];
        }
        //if approved
        if (isset($res["status"]) && $res["status"] == "APPROVED") {
            $return_array["error"] = 0;
            $return_array["message"] = $res["status"];
        }

        return $return_array;
    }

    /**
     * Send  merchent refference to  Scalapay after order success
     *
     * @return bool
     *
     */
    public function scalapayUpdateMerchentRefAfterOrder($orderToken, $merchantOrder)
    {
        $return_array = [];
        $urlKey = "v2/orders/" . $orderToken;
        $data = ["merchantReference" => $merchantOrder];
        $data = json_encode($data);
        $response = $this->getCurlHandler($urlKey, $data);
        $res = $this->_jsonHelper->jsonDecode($response);
        $this->log("scalapay update merchant ref res: ");
        $this->log($res);

        return $res;
    }

    /**
    * Check the status of a payment at Scalapay
    *
    * @param string|null $orderToken
    *
    * @return array
    */
    public function scalapayPaymentStatus($orderToken)
    {
        $return_array = [];
        $urlKey = "v2/payments/" . $orderToken;
        $response = $this->getCurlHandler($urlKey);
        $res = $this->_jsonHelper->jsonDecode($response);

        if (isset($res["errorCode"]) && $res["errorCode"] != "") {
            $return_array["error"] = 1;
            $return_array["message"] = $res["message"];
        }
        //if approved
        if (isset($res["status"]) && ($res["status"] == "charged" ||   $res["status"] == "partially_refunded")) {
            $return_array["error"] = 0;
            $return_array["status"] = $res["status"];
            $return_array["total_amount"] = $res["totalAmount"];
            $return_array["order_details"] = $res["orderDetails"];
            $return_array["created"] = $res["created"];
            $return_array["message"] = "";
        } else {
            $return_array["error"] = 1;
            if (isset($res["message"])) {
                $return_array["message"] = $res["message"];
            } else {
                $return_array["message"] = "";
            }
            $return_array["status"] = "";
            $return_array["total_amount"] = "";
            $return_array["order_details"] = "";
            $return_array["created"] = "";
        }

        return $return_array;
    }

    /**
    * get the payload of a payment at Scalapay
    *
    * @param string|null $orderToken
    *
    * @return array
    */
    public function scalapayGetPayloadData($orderToken)
    {
        $return_array = [];
        $urlKey = "v2/payments/" . $orderToken;
        $response = $this->getCurlHandler($urlKey);
        $res = $this->_jsonHelper->jsonDecode($response);

        $return_array["error"] = 1;
        $return_array["billing"] = "";
        $return_array["shipping"] = "";

        //if approved
        if ($res["orderDetails"] !== null) {
            $return_array["error"] = 0;
            $return_array["billing"] = $res["orderDetails"]["billing"];
            $return_array["shipping"] = $res["orderDetails"]["shipping"];
            $return_array["consumer"] = $res["orderDetails"]["consumer"];
        }

        return $return_array;
    }

    /**
    * Read response for Scalapay curl request while order Capture
    *
    * @param string|null $orderToken
    * @param string|null $refundAmmount
    * @param string|null $currency
    * @return array
    */
    public function scalapayRefundPayment($orderToken, $merchantOrder, $refundAmount, $currency)
    {
        $return_array = [];
        $urlKey = "v2/payments/" . $orderToken . "/refund";
        $this->_curl->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->_curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $data = [
                "refundAmount" => [
                                    "amount" => "$refundAmount",
                                    "currency" => "$currency"
                                  ],
                "merchantReference" => $merchantOrder,
                "merchantRefundReference" => "R" . $merchantOrder
                ];
        $data = json_encode($data);
        $response = $this->getCurlHandler($urlKey, $data);
        $res = $this->_jsonHelper->jsonDecode($response);
        $this->log("scalapayRefundPayment res: ");
        $this->log($res);

        if (isset($res["errorCode"]) && $res["errorCode"] != "") {
            $return_array["error"] = 1;
            $return_array["errorId"] = $res["errorId"];
            $return_array["errorCode"] = $res["errorCode"];
            $return_array["message"] = $res["message"];
        }
        if (isset($res["refundToken"])) {
            $return_array["error"] = 0;
            $return_array["refundToken"] = $res["refundToken"];
            $return_array["refundedAt"] = $res["refundedAt"];
        }

        return $return_array;
    }

    /**
     * Check sclalapay payment method is active
     * @param string|int $amount
     * @param array $products
     * @param string|null $currentCountry
     * @return array
     *
     */
    public function isScalapayActive($amount, $products = array(), $currentCountry = "", $area = null)
    {
        $isEnabled  = $this->getConfigData("active");
        if (!$isEnabled) {
            return false;
        }
        $configData = $this->getScalapayConfig();
        $allowIfLessValue = false;


        if (
            $this->getConfigData('product_apply_min_order_value') == 0 &&
            $area == 'product' &&
            $amount < $configData["minimumAmount"]
        ) {
            $allowIfLessValue = true;
        } elseif (
            $this->getConfigData('cart_apply_min_order_value') == 0 &&
            $area == 'cart' &&
            $amount < $configData["minimumAmount"]
        ) {
            $allowIfLessValue = true;
        }

        // checking if order total or product price is with in specified limits.
        if (
            ($amount < $configData["minimumAmount"] && $allowIfLessValue == false) ||
            $amount > $configData["maximumAmount"]
        ) {
            return false;
        }

        if (count($products) > 0) {
            $disabled_categories = $this->getConfigData('disabled_categories');
            $filter_categories = [];
            if (isset($disabled_categories) && $disabled_categories != "") {
                $categories = explode(",", $disabled_categories);
                if (count($categories) > 0) {
                    foreach ($categories as $c) {
                        try {
                            $category = $this->_categoryRepository->get($c);
                            $sub_categories = $category->getAllChildren(true);
                            $filter_categories[] = $category->getId();
                            if (count($sub_categories) > 0) {
                                $filter_categories = array_merge($sub_categories, $filter_categories);
                            }
                        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                            $this->log("category id not exist: " . $c);
                        }
                    }
                }
            }
            foreach ($products as $pid) {
                if ($pid > 0 && $pid != "") {
                    $p = $this->_productFactory->create()->load($pid);
                    if ($p->getId()) {
                        if (count($filter_categories) > 0) {
                            $productCategoryIds = $p->getCategoryIds();
                            $result = array_intersect($productCategoryIds, $filter_categories);
                            if (count($result) > 0) {
                                return false;
                            }
                        }
                        if ($p->getIsVirtual()) {
                            return false;
                        }
                    }
                }
            }
        }
        $allowSpecificCountries = $this->getConfigData("allowspecific");
        $countries = [];
        if ($allowSpecificCountries == 1) {
            $specificCountries = $this->getConfigData("specificcountry");
            if (isset($specificCountries)) {
                $countries = explode(",", $specificCountries);
            }
        }

        $currentLanguage = $this->_storeResolver->getLocale();
        $allowSpecificLanguages = $this->getConfigData("allowspecificlanguages");
        $languages = [];
        if ($allowSpecificLanguages == 1) {
            $specificLanguages = $this->getConfigData("specificlanguages");
            if (isset($specificLanguages)) {
                $languages = explode(",", $specificLanguages);
            }
        }

        $currentCurrency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $allowSpecificCurrencies = $this->getConfigData("allowspecificcurrencies");
        $currencies = [];
        if ($allowSpecificCurrencies == 1) {
            $specificCurrencies = $this->getConfigData("specificcurrencies");
            if (isset($specificCurrencies)) {
                $currencies = explode(",", $specificCurrencies);
            }
        }

        if (
            (
                $allowSpecificCountries == 0 ||
                (
                    $allowSpecificCountries == 1 &&
                    (
                        in_array($currentCountry, $countries) ||
                        $currentCountry == ""
                    )
                )
            ) &&
            (
                $allowSpecificLanguages == 0 ||
                (
                    $allowSpecificLanguages == 1 &&
                    (
                        in_array($currentLanguage, $languages) ||
                        $currentLanguage == ""
                    )
                )
            ) &&
            (
                $allowSpecificCurrencies == 0 ||
                (
                    $allowSpecificCurrencies == 1 &&
                    (
                        in_array($currentCurrency, $currencies) ||
                        $currentCurrency == ""
                    )
                )
            )
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check sclalapay payment method is active
     * @param string|int $amount
     * @param array $products
     * @param string|null $currentCountry
     * @return array
     *
     */
    public function isScalapayPayin4Active($amount, $products = array(), $currentCountry = "", $area = null)
    {
        $isEnabled  = $this->getConfigDataPayin4("active");
        if (!$isEnabled) {
            return false;
        }
        $configData = $this->getScalapayPayin4Config();
        $allowIfLessValue = false;

        if (
            $this->getConfigDataPayin4('product_apply_min_order_value') == 0 &&
             $area == 'product' &&
             $amount < $configData["minimumAmount"]
        ) {
            $allowIfLessValue = true;
        } elseif (
            $this->getConfigDataPayin4('cart_apply_min_order_value') == 0 &&
            $area == 'cart' &&
             $amount < $configData["minimumAmount"]
        ) {
            $allowIfLessValue = true;
        }

        // checking if order total or product price is with in specified limits.
        if (
            ($amount < $configData["minimumAmount"] && $allowIfLessValue == false) ||
            $amount > $configData["maximumAmount"]
        ) {
            return false;
        }

        if (count($products) > 0) {
            $disabled_categories = $this->getConfigDataPayin4('disabled_categories');
            $filter_categories = [];
            if (isset($disabled_categories) && $disabled_categories != "") {
                $categories = explode(",", $disabled_categories);
                if (count($categories) > 0) {
                    foreach ($categories as $c) {
                        try {
                            $category = $this->_categoryRepository->get($c);
                            $sub_categories = $category->getAllChildren(true);
                            $filter_categories[] = $category->getId();
                            if (count($sub_categories) > 0) {
                                $filter_categories = array_merge($sub_categories, $filter_categories);
                            }
                        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                            $this->log("category id not exist: " . $c);
                        }
                    }
                }
            }
            foreach ($products as $pid) {
                if ($pid > 0 && $pid != "") {
                    $p = $this->_productFactory->create()->load($pid);
                    if ($p->getId()) {
                        if (count($filter_categories) > 0) {
                            $productCategoryIds = $p->getCategoryIds();
                            $result = array_intersect($productCategoryIds, $filter_categories);
                            if (count($result) > 0) {
                                return false;
                            }
                        }
                        if ($p->getIsVirtual()) {
                            return false;
                        }
                    }
                }
            }
        }
        $allowSpecificCountries = $this->getConfigDataPayin4("allowspecific");
        $countries = [];
        if ($allowSpecificCountries == 1) {
            $specificCountries = $this->getConfigDataPayin4("specificcountry");
            if (isset($specificCountries)) {
                $countries = explode(",", $specificCountries);
            }
        }

        $currentLanguage = $this->_storeResolver->getLocale();
        $allowSpecificLanguages = $this->getConfigDataPayin4("allowspecificlanguages");
        $languages = [];
        if ($allowSpecificLanguages == 1) {
            $specificLanguages = $this->getConfigDataPayin4("specificlanguages");
            if (isset($specificLanguages)) {
                $languages = explode(",", $specificLanguages);
            }
        }

        $currentCurrency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $allowSpecificCurrencies = $this->getConfigDataPayin4("allowspecificcurrencies");
        $currencies = [];
        if ($allowSpecificCurrencies == 1) {
            $specificCurrencies = $this->getConfigDataPayin4("specificcurrencies");
            if (isset($specificCurrencies)) {
                $currencies = explode(",", $specificCurrencies);
            }
        }

        if (
            (
                $allowSpecificCountries == 0 ||
                (
                    $allowSpecificCountries == 1 &&
                    (
                        in_array($currentCountry, $countries) ||
                        $currentCountry == ""
                    )
                )
            ) &&
            (
                $allowSpecificLanguages == 0 ||
                (
                    $allowSpecificLanguages == 1 &&
                    (
                        in_array($currentLanguage, $languages) ||
                        $currentLanguage == ""
                    )
                )
            ) &&
            (
                $allowSpecificCurrencies == 0 ||
                (
                    $allowSpecificCurrencies == 1 &&
                    (
                        in_array($currentCurrency, $currencies) ||
                        $currentCurrency == ""
                    )
                )
            )
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check sclalapay payment method is active
     * @param string|int $amount
     * @param array $products
     * @param string|null $currentCountry
     * @return array
     *
     */
    public function isScalapayPayLaterActive($amount, $products = array(), $currentCountry = "", $area = null)
    {
        $isEnabled  = $this->getConfigDataPayLater("active");
        if (!$isEnabled) {
            return false;
        }
        $configData = $this->getScalapayPayLaterConfig();
        $allowIfLessValue = false;

        if (
            $this->getConfigDataPayLater('product_apply_min_order_value') == 0 &&
            $area == 'product' &&
            $amount < $configData["minimumAmount"]
        ) {
            $allowIfLessValue = true;
        } elseif (
            $this->getConfigDataPayLater('cart_apply_min_order_value') == 0 &&
            $area == 'cart' &&
            $amount < $configData["minimumAmount"]
        ) {
            $allowIfLessValue = true;
        }

        // checking if order total or product price is with in specified limits.
        if (
            ($amount < $configData["minimumAmount"] && $allowIfLessValue == false) ||
            $amount > $configData["maximumAmount"]
        ) {
            return false;
        }

        if (count($products) > 0) {
            $disabled_categories = $this->getConfigDataPayLater('disabled_categories');
            $filter_categories = [];
            if (isset($disabled_categories) && $disabled_categories != "") {
                $categories = explode(",", $disabled_categories);
                if (count($categories) > 0) {
                    foreach ($categories as $c) {
                        try {
                            $category = $this->_categoryRepository->get($c);
                            $sub_categories = $category->getAllChildren(true);
                            $filter_categories[] = $category->getId();
                            if (count($sub_categories) > 0) {
                                $filter_categories = array_merge($sub_categories, $filter_categories);
                            }
                        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                            $this->log("category id not exist: " . $c);
                        }
                    }
                }
            }
            foreach ($products as $pid) {
                if ($pid > 0 && $pid != "") {
                    $p = $this->_productFactory->create()->load($pid);
                    if ($p->getId()) {
                        if (count($filter_categories) > 0) {
                            $productCategoryIds = $p->getCategoryIds();
                            $result = array_intersect($productCategoryIds, $filter_categories);
                            if (count($result) > 0) {
                                return false;
                            }
                        }
                        if ($p->getIsVirtual()) {
                            return false;
                        }
                    }
                }
            }
        }
        $allowSpecificCountries = $this->getConfigDataPayLater("allowspecific");
        $countries = [];
        if ($allowSpecificCountries == 1) {
            $specificCountries = $this->getConfigDataPayLater("specificcountry");
            if (isset($specificCountries)) {
                $countries = explode(",", $specificCountries);
            }
        }

        $currentLanguage = $this->_storeResolver->getLocale();
        $allowSpecificLanguages = $this->getConfigDataPayLater("allowspecificlanguages");
        $languages = [];
        if ($allowSpecificLanguages == 1) {
            $specificLanguages = $this->getConfigDataPayLater("specificlanguages");
            if (isset($specificLanguages)) {
                $languages = explode(",", $specificLanguages);
            }
        }

        $currentCurrency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $allowSpecificCurrencies = $this->getConfigDataPayLater("allowspecificcurrencies");
        $currencies = [];
        if ($allowSpecificCurrencies == 1) {
            $specificCurrencies = $this->getConfigDataPayLater("specificcurrencies");
            if (isset($specificCurrencies)) {
                $currencies = explode(",", $specificCurrencies);
            }
        }

        if (
            (
                $allowSpecificCountries == 0 ||
                (
                    $allowSpecificCountries == 1 &&
                    (
                        in_array($currentCountry, $countries) ||
                        $currentCountry == ""
                    )
                )
            ) &&
            (
                $allowSpecificLanguages == 0 ||
                (
                    $allowSpecificLanguages == 1 &&
                    (
                        in_array($currentLanguage, $languages) ||
                        $currentLanguage == ""
                    )
                )
            ) &&
            (
                $allowSpecificCurrencies == 0 ||
                (
                    $allowSpecificCurrencies == 1 &&
                    (
                        in_array($currentCurrency, $currencies) ||
                        $currentCurrency == ""
                    )
                )
            )
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     *
     * @return string
     */
    public function getConfigData($field, $useFullPath = 0)
    {
        $value = "";
        if ($field != "") {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

            if ($useFullPath === 0) {
                switch ($field) {
                    case 'active':
                    case 'title':
                    case 'order_status':
                    case 'allowspecific':
                    case 'specificcountry':
                    case 'allowspecificlanguages':
                    case 'specificlanguages':
                    case 'allowspecificcurrencies':
                    case 'specificcurrencies':
                    case 'scalapay_failed_payment_error':
                    case 'payment_action':
                    case 'sort_order':
                        $path = 'payment/scalapay/' . $field;
                        break;
                    case 'mode':
                    case 'test_url':
                    case 'production_url':
                    case 'test_api_password':
                    case 'production_api_password':
                    case 'site_url':
                    case 'installment_description_css':
                    case 'disabled_categories':
                        $path = 'scalapay/settings/' . $field;
                        break;
                    case 'min_amount':
                    case 'max_amount':
                    case 'number_of_payments':
                        $path = 'scalapay/payin3/restrictions/' . $field;
                        break;
                    case 'product_text_size':
                    case 'product_logo_size':
                    case 'product_price_color':
                    case 'product_logo_color':
                    case 'product_logo_alignment':
                    case 'product_amount_selector_array':
                    case 'product_pricebox_selector':
                    case 'product_show_widget':
                    case 'product_text_position':
                    case 'product_show_logo':
                    case 'product_apply_min_order_value':
                    case 'product_hide_price':
                    case 'product_currency_position':
                    case 'product_currency_display':
                    case 'product_theme':
                    case 'product_scalapay_enable_below_widget_text':
                    case 'product_scalapay_below_widget_text':
                        $path = 'scalapay/payin3/product/' . $field;
                        break;
                    case 'cart_text_size':
                    case 'cart_logo_size':
                    case 'cart_price_color':
                    case 'cart_logo_color':
                    case 'cart_logo_alignment':
                    case 'cart_amount_selector_array':
                    case 'cart_pricebox_selector':
                    case 'cart_show_widget':
                    case 'cart_text_position':
                    case 'cart_show_logo':
                    case 'cart_apply_min_order_value':
                    case 'cart_hide_price':
                    case 'cart_currency_position':
                    case 'cart_currency_display':
                    case 'cart_theme':
                        $path = 'scalapay/payin3/cart/' . $field;
                        break;

                    case 'checkout_text_size':
                    case 'checkout_logo_size':
                    case 'checkout_price_color':
                    case 'checkout_logo_color':
                    case 'checkout_logo_alignment':
                    case 'checkout_amount_selector_array':
                    case 'checkout_pricebox_selector':
                    case 'checkout_show_widget':
                    case 'checkout_show_title':
                    case 'checkout_text_position':
                    case 'checkout_show_logo':
                    case 'checkout_apply_min_order_value':
                    case 'checkout_hide_price':
                    case 'checkout_currency_position':
                    case 'checkout_currency_display':
                    case 'checkout_theme':
                        $path = 'scalapay/payin3/checkout/' . $field;
                        break;
                    case 'installment_checkout_description':
                    case 'installment_description':
                    case 'scalapay_logo_css':
                        $path = 'scalapay/payin3/checkout_installment/' . $field;
                        break;

                    default:
                        $path = 'payment/scalapay/' . $field;
                        break;
                }
            } else {
                $path = $field;
            }

            $value = $this->_scopeConfig->getValue($path, $storeScope);

            switch ($field) {
                case 'installment_checkout_description':
                case 'installment_description':
                case 'title':
                case 'scalapay_error_message':
                    $value = __($value);
                    break;
            }
        }

        return $value;
    }

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     *
     * @return string
     */
    public function getConfigDataPayin4($field, $useFullPath = 0)
    {
        $value = "";
        if ($field != "") {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

            if ($useFullPath === 0) {
                switch ($field) {
                    case 'active':
                    case 'title':
                    case 'order_status':
                    case 'allowspecific':
                    case 'specificcountry':
                    case 'allowspecificlanguages':
                    case 'specificlanguages':
                    case 'allowspecificcurrencies':
                    case 'specificcurrencies':
                    case 'scalapay_failed_payment_error':
                    case 'payment_action':
                    case 'sort_order':
                        $path = 'payment/payin4/' . $field;
                        break;
                    case 'mode':
                    case 'test_url':
                    case 'production_url':
                    case 'test_api_password':
                    case 'production_api_password':
                    case 'site_url':
                    case 'installment_description_css':
                    case 'disabled_categories':
                        $path = 'scalapay/settings/' . $field;
                        break;
                    case 'min_amount':
                    case 'max_amount':
                    case 'number_of_payments':
                        $path = 'scalapay/payin4/restrictions/' . $field;
                        break;
                    case 'product_text_size':
                    case 'product_logo_size':
                    case 'product_price_color':
                    case 'product_logo_color':
                    case 'product_logo_alignment':
                    case 'product_amount_selector_array':
                    case 'product_pricebox_selector':
                    case 'product_show_widget':
                    case 'product_text_position':
                    case 'product_show_logo':
                    case 'product_apply_min_order_value':
                    case 'product_hide_price':
                    case 'product_scalapay_enable_below_widget_text':
                    case 'product_scalapay_below_widget_text':
                    case 'product_currency_position':
                    case 'product_currency_display':
                    case 'product_theme':
                        $path = 'scalapay/payin4/product/' . $field;
                        break;
                    case 'cart_text_size':
                    case 'cart_logo_size':
                    case 'cart_price_color':
                    case 'cart_logo_color':
                    case 'cart_logo_alignment':
                    case 'cart_amount_selector_array':
                    case 'cart_pricebox_selector':
                    case 'cart_show_widget':
                    case 'cart_text_position':
                    case 'cart_show_logo':
                    case 'cart_apply_min_order_value':
                    case 'cart_hide_price':
                    case 'cart_currency_position':
                    case 'cart_currency_display':
                    case 'cart_theme':
                        $path = 'scalapay/payin4/cart/' . $field;
                        break;
                    case 'checkout_text_size':
                    case 'checkout_logo_size':
                    case 'checkout_price_color':
                    case 'checkout_logo_color':
                    case 'checkout_logo_alignment':
                    case 'checkout_amount_selector_array':
                    case 'checkout_pricebox_selector':
                    case 'checkout_show_widget':
                    case 'checkout_show_title':
                    case 'checkout_text_position':
                    case 'checkout_show_logo':
                    case 'checkout_apply_min_order_value':
                    case 'checkout_hide_price':
                    case 'checkout_currency_position':
                    case 'checkout_currency_display':
                    case 'checkout_theme':
                        $path = 'scalapay/payin4/checkout/' . $field;
                        break;
                    case 'installment_checkout_description':
                    case 'installment_description':
                    case 'scalapay_logo_css':
                        $path = 'scalapay/payin4/checkout_installment/' . $field;
                        break;

                    default:
                        $path = 'payment/scalapay/' . $field;
                        break;
                }
            } else {
                $path = $field;
            }

            $value = $this->_scopeConfig->getValue($path, $storeScope);

            switch ($field) {
                case 'installment_checkout_description':
                case 'installment_description':
                case 'title':
                case 'scalapay_error_message':
                    $value = __($value);
                    break;
            }
        }

        return $value;
    }

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     *
     * @return string
     */
    public function getConfigDataPayLater($field, $useFullPath = 0)
    {
        $value = "";
        if ($field != "") {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

            if ($useFullPath === 0) {
                switch ($field) {
                    case 'active':
                    case 'title':
                    case 'order_status':
                    case 'allowspecific':
                    case 'specificcountry':
                    case 'allowspecificlanguages':
                    case 'specificlanguages':
                    case 'allowspecificcurrencies':
                    case 'specificcurrencies':
                    case 'scalapay_failed_payment_error':
                    case 'payment_action':
                    case 'number_of_days':
                    case 'sort_order':
                        $path = 'payment/paylater/' . $field;
                        break;
                    case 'mode':
                    case 'test_url':
                    case 'production_url':
                    case 'test_api_password':
                    case 'production_api_password':
                    case 'site_url':
                    case 'installment_description_css':
                    case 'disabled_categories':
                        $path = 'scalapay/settings/' . $field;
                        break;
                    case 'min_amount':
                    case 'max_amount':
                    case 'number_of_payments':
                        $path = 'scalapay/paylater/restrictions/' . $field;
                        break;
                    case 'product_text_size':
                    case 'product_logo_size':
                    case 'product_price_color':
                    case 'product_logo_color':
                    case 'product_logo_alignment':
                    case 'product_amount_selector_array':
                    case 'product_pricebox_selector':
                    case 'product_show_widget':
                    case 'product_text_position':
                    case 'product_show_logo':
                    case 'product_apply_min_order_value':
                    case 'product_hide_price':
                    case 'product_scalapay_enable_below_widget_text':
                    case 'product_scalapay_below_widget_text':
                    case 'product_currency_position':
                    case 'product_currency_display':
                    case 'product_theme':
                        $path = 'scalapay/paylater/product/' . $field;
                        break;
                    case 'cart_text_size':
                    case 'cart_logo_size':
                    case 'cart_price_color':
                    case 'cart_logo_color':
                    case 'cart_logo_alignment':
                    case 'cart_amount_selector_array':
                    case 'cart_pricebox_selector':
                    case 'cart_show_widget':
                    case 'cart_text_position':
                    case 'cart_show_logo':
                    case 'cart_apply_min_order_value':
                    case 'cart_hide_price':
                    case 'cart_currency_position':
                    case 'cart_currency_display':
                    case 'cart_theme':
                        $path = 'scalapay/paylater/cart/' . $field;
                        break;
                    case 'checkout_text_size':
                    case 'checkout_logo_size':
                    case 'checkout_price_color':
                    case 'checkout_logo_color':
                    case 'checkout_logo_alignment':
                    case 'checkout_amount_selector_array':
                    case 'checkout_pricebox_selector':
                    case 'checkout_show_widget':
                    case 'checkout_show_title':
                    case 'checkout_text_position':
                    case 'checkout_show_logo':
                    case 'checkout_apply_min_order_value':
                    case 'checkout_hide_price':
                    case 'checkout_currency_position':
                    case 'checkout_currency_display':
                    case 'checkout_theme':
                        $path = 'scalapay/paylater/checkout/' . $field;
                        break;
                    case 'installment_checkout_description':
                    case 'installment_description':
                    case 'scalapay_logo_css':
                        $path = 'scalapay/paylater/checkout_installment/' . $field;
                        break;

                    default:
                        $path = 'payment/scalapay/' . $field;
                        break;
                }
            } else {
                $path = $field;
            }

            $value = $this->_scopeConfig->getValue($path, $storeScope);

            switch ($field) {
                case 'installment_checkout_description':
                case 'installment_description':
                case 'title':
                case 'scalapay_error_message':
                    $value = __($value);
                    break;
            }
        }

        return $value;
    }

    /*
    * Log writer to track bug
    *@param $log_string string | array
    */
    public function log($log_string)
    {
        if (isset($log_string)) {
            $oldFileName = $this->getLogFileName("-2 months");

            if (file_exists($oldFileName)) {
                unlink($oldFileName);
            }

            $filename = $this->getLogFileName();
            try {
                if (version_compare($this->_productMetadataInterface->getVersion(), '2.4.2', '<=')) {
                    //version is 2.4.2 or below
                    $writer = new \Zend\Log\Writer\Stream($filename);
                    $logger = new \Zend\Log\Logger();
                    $logger->addWriter($writer);

                    if (!is_array($log_string) && !is_object($log_string)) {
                        $logger->info($log_string);
                    } else {
                        $logger->info(print_r($log_string, true));
                    }
                } else {
                    //version is higher 2.4.2
                    $writer = new \Zend_Log_Writer_Stream($filename);
                    $logger = new \Zend_Log();
                    $logger->addWriter($writer);

                    if (!is_array($log_string) && !is_object($log_string)) {
                        $logger->info($log_string);
                    } else {
                        $logger->info(print_r($log_string, true));
                    }
                }
            } catch (\Exception $e) {
                $e->getMessage();
            }
        }
    }

    public function getLogFileName($fileDate = "")
    {
        $basePath = \Magento\Framework\App\Filesystem\DirectoryList::getDefaultConfig();

        $filename = "";
        $logDir = BP . "/" . $basePath['log']['path'];

        if (isset($fileDate) && $fileDate !== "") {
            $filename = $logDir . '/scalapay_' . $this->_datetime->date('Y_m', $fileDate) . '.log';
        } else {
            $filename = $logDir . '/scalapay_' . $this->_datetime->date('Y_m') . '.log';
        }

        return $filename;
    }

    /**
     * Prepare and format Scalapay text.
     *
     * @param sting $areakey
     * @param string $price
     * @param string $logo
     *
     * @return string
     */
    public function getScalapayFormatText($areakey, $price, $logo)
    {
        $price = $this->getScalapayFormatedPrice($price);
        $nop = 3;
        $price = '<span id="scalapay_ins">' . $price . '</span>';
        $description = $this->getConfigData($areakey);
        $description = str_replace("<price>", $price, $description);
        $description = str_replace("<emoji>", "ðŸ˜Š", $description);
        $description = str_replace("<logo>", $logo, $description);
        $description = str_replace("<nop>", $nop, $description);

        return $description;
    }

    /**
     * Prepare and format Scalapay text.
     *
     * @param sting $areakey
     * @param string $price
     * @param string $logo
     *
     * @return string
     */
    public function getScalapayPayin4FormatText($areakey, $price, $logo)
    {
        $price = $this->getScalapayFormatedPrice($price);
        $nop = 4;
        $price = '<span id="scalapay_payin4_ins">' . $price . '</span>';
        $description = $this->getConfigDataPayin4($areakey);
        $description = str_replace("<price>", $price, $description);
        $description = str_replace("<emoji>", "ðŸ˜Š", $description);
        $description = str_replace("<logo>", $logo, $description);
        $description = str_replace("<nop>", $nop, $description);

        return $description;
    }
    /**
     * Prepare and format Scalapay text.
     *
     * @param sting $areakey
     * @param string $price
     * @param string $logo
     *
     * @return string
     */
    public function getScalapayPayLaterFormatText($areakey, $price, $logo)
    {
        $price = $this->getScalapayFormatedPrice($price);
        $nop = 1;
        $price = '<span id="scalapay_ins">' . $price . '</span>';
        $description = $this->getConfigDataPayLater($areakey);
        $description = str_replace("<price>", $price, $description);
        $description = str_replace("<emoji>", "ðŸ˜Š", $description);
        $description = str_replace("<logo>", $logo, $description);
        $description = str_replace("<nop>", $nop, $description);

        return $description;
    }

    /**
     * Prepare and format Scalapay payment description for Checkout page.
     * @return string
     */
    public function getScalapayCheckoutInstallmentDescription()
    {
        $description = $this->getConfigData('installment_description');
        $nop = 3;

        return str_replace("<nop>", $nop, $description);
    }

    /**
     * Prepare and format Scalapay payment description for Checkout page.
     * @return string
     */
    public function getScalapayPayin4CheckoutInstallmentDescription()
    {
        $description = $this->getConfigDataPayin4('installment_description');
        $nop = 4;

        return str_replace("<nop>", $nop, $description);
    }

    /**
    * Prepare and format Scalapay payment description for Checkout page.
    * @return string
    */
    public function getScalapayPayLaterCheckoutInstallmentDescription()
    {
        $description = $this->getConfigDataPayLater('installment_description');
        $nop = 1;

        return str_replace("<nop>", $nop, $description);
    }

    /**
     * Prepare and format Scalapay text for Checkout page.
     *
     * @param string $price
     *
     * @return string
     */
    public function getScalapayCheckoutPageText($price)
    {
        if ($this->getCurrentLanguage() == 'it') {
            $logo = '<span class="scalapay_instalments_checkout_logo_italy"></span>';
        } else {
            $logo = '<span class="scalapay_instalments_checkout_logo"></span>';
        }

        return  $this->getScalapayFormatText("installment_checkout_description", $price, $logo);
    }

    /**
     * Prepare and format Scalapay text for Cart page.
     *
     * @param string $price
     *
     * @return string
     */
    public function getScalapayCartPageText($price)
    {
        $logo = '<span class="scalapay_instalments_product_logo"></span>';

        return  $this->getScalapayFormatText("installment_cart_description", $price, $logo);
    }

    /**
    * Prepare and format Scalapay text for Checkout page.
    *
    * @param string $price
    *
    * @return string
    */
    public function getScalapayPayin4CheckoutPageText($price)
    {
        if ($this->getCurrentLanguage() == 'it') {
            $logo = '<span class="scalapay_instalments_checkout_logo_italy"></span>';
        } else {
            $logo = '<span class="scalapay_instalments_checkout_logo"></span>';
        }

        return  $this->getScalapayPayin4FormatText("installment_checkout_description", $price, $logo);
    }

    /**
     * Prepare and format Scalapay text for Cart page.
     *
     * @param string $price
     *
     * @return string
     */
    public function getScalapayPayin4CartPageText($price)
    {
        $logo = '<span class="scalapay_instalments_product_logo"></span>';

        return  $this->getScalapayPayin4FormatText("installment_cart_description", $price, $logo);
    }

    /**
    * Prepare and format Scalapay text for Checkout page.
    *
    * @param string $price
    *
    * @return string
    */
    public function getScalapayPayLaterCheckoutPageText($price)
    {
        if ($this->getCurrentLanguage() == 'it') {
            $logo = '<span class="scalapay_instalments_checkout_logo_italy"></span>';
        } else {
            $logo = '<span class="scalapay_instalments_checkout_logo"></span>';
        }

        return  $this->getScalapayPayLaterFormatText("installment_checkout_description", $price, $logo);
    }

    /**
     * Prepare and format Scalapay text for Cart page.
     *
     * @param string $price
     *
     * @return string
     */
    public function getScalapayPayLaterCartPageText($price)
    {
        $logo = '<span class="scalapay_instalments_product_logo"></span>';

        return  $this->getScalapayPayLaterFormatText("installment_cart_description", $price, $logo);
    }

    public function processRegionId($region_id, $state, $country_code)
    {
        $regionFactory = $this->_regionFactory->create();
        try {
            $region = $regionFactory->load($region_id);
            if ($region->getRegionId() !== null) {
                $this->log($region->getData());
                $region_id = $region->getRegionId();
            } else {
                $this->log("Region id in else:");
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Region id is not exist.'));
                $region = $regionFactory->loadByName($state, $country_code);
                if ($region->getRegionId() !== null) {
                    $this->log("Get region id by name:");
                    $this->log($region->getData());
                    $region_id = $region->getRegionId();
                }
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $this->log("NoSuchEntity Exception: state: " . $state . ", country code: " . $country_code);
            $region = $regionFactory->loadByName($state, $country_code);
            $region_id = "";
            if ($region->getRegionId() !== null) {
                $this->log($region->getData());
                $region_id = $region->getRegionId();
            }
            $this->log($region->getData());
            $this->log("NoSuchEntityException: " . $e->getMessage());
        } catch (\Exception $e) {
            $region_id = "";
            $this->log("Exception: " . $e->getMessage());
        }

        return $region_id;
    }

    /**
     * Prepare and format Scalapay text for product page.
     *
     * @param string $price
     *
     * @return string
     */
    public function getScalapayProductPageText($price)
    {
        $logo = '<span class="scalapay_instalments_product_logo"></span>';

        return   $this->getScalapayFormatText("installment_product_description", $price, $logo);
    }
    /**
     * Prepare and format Scalapay text for product page.
     *
     * @param string $price
     *
     * @return string
     */
    public function getScalapayPayin4ProductPageText($price)
    {
        $logo = '<span class="scalapay_instalments_product_logo"></span>';

        return   $this->getScalapayPayin4FormatText("installment_product_description", $price, $logo);
    }
    /**
     * Prepare and format Scalapay text for product page.
     *
     * @param string $price
     *
     * @return string
     */
    public function getScalapayPayLaterProductPageText($price)
    {
        $logo = '<span class="scalapay_instalments_product_logo"></span>';

        return   $this->getScalapayPayLaterFormatText("installment_product_description", $price, $logo);
    }
    /**
     * Prepare and format Scalapay text.
     *
     * @param sting $price
     *
     * @return string
     */
    public function getScalapayFormatedPrice($price)
    {
        if ($price) {
            $price = $this->_priceHelper->currency($price, true, false);
        }

        return $price;
    }

    public function epsilonCalculation($value1, $value2)
    {
        $epsilon = 0.01;
        $this->log("value 1: " . $value1);
        $this->log("value 2: " . $value2);
        if (abs($value1 - $value2) < $epsilon) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the product thumbnail url.
     *
     * @param $productId
     * @return string
     */
    private function getProductThumbnailUrl($productId)
    {
        try {
            $product = $this->_productRepositoryInterfaceFactory->create()->getById($productId);
            return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) .
                'catalog/product' .
                $product->getThumbnail();
        } catch (\Exception $e) {
            return '';
        }
    }

    public function ctrim($wordToTrim)
    {
        return trim(isset($wordToTrim) ? $wordToTrim : "");
    }

    public function crtrim($wordToRTrim, $sep = "")
    {
        return rtrim(isset($wordToRTrim) ? $wordToRTrim : "", $sep);
    }
}
