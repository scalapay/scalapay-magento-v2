/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */

define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils',
    'Magento_Checkout/js/model/totals',
    'jquery'
], function (Component, quote, priceUtils, totals,$) {
    'use strict';

    return Component.extend({
        defaults: {
            isFullTaxSummaryDisplayed: window.checkoutConfig.isFullTaxSummaryDisplayed || false,
            template: 'Magento_Tax/checkout/summary/grand-total'
        },
        totals: quote.getTotals(),
        isTaxDisplayedInGrandTotal: window.checkoutConfig.includeTaxInGrandTotal || false,

        /**
         * @return {*}
         */
        isDisplayed: function () {
            return this.isFullMode();
        },

        /**
         * @return {*|String}
         */
        getValue: function () {
            var price = 0;

            if (this.totals()) {
                price = totals.getSegment('grand_total').value;
            }

            var is_scalapay = window.checkoutConfig.payment.scalapayconfig;

            if (typeof is_scalapay !== 'undefined') {
                var number_of_ins = window.checkoutConfig.payment.scalapayconfig["scalapay"];
                var num = price / number_of_ins;
                var n = num.toFixed(2);

                n = priceUtils.formatPrice(n);



                $("body").loader("show");

                setTimeout(
                    function () {

                        var elem = document.getElementById("scalapay_ins");
                        if (typeof elem !== 'undefined' && elem !== null) {
                            n = n.replace(".", ",");
                            document.getElementById("scalapay_ins").innerHTML = n;
                            $("body").loader("hide");
                        } else {
                            $("body").loader("hide");
                        }

                    },
                    5000
                );
            }

            return this.getFormattedPrice(price);
        },
        /**
         * @return {*|String}
         */
        getBaseValue: function () {
            var price = 0;

            if (this.totals()) {
                price = this.totals()['base_grand_total'];
            }

            return priceUtils.formatPrice(price, quote.getBasePriceFormat());
        },

        /**
         * @return {*}
         */
        getGrandTotalExclTax: function () {
            var total = this.totals();

            if (!total) {
                return 0;
            }

            return this.getFormattedPrice(total['grand_total']);
        },

        /**
         * @return {Boolean}
         */
        isBaseGrandTotalDisplayNeeded: function () {
            var total = this.totals();

            if (!total) {
                return false;
            }

            return total['base_currency_code'] != total['quote_currency_code']; //eslint-disable-line eqeqeq
        }
    });
});
