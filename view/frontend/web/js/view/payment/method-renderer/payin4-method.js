/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
    "Magento_Checkout/js/view/payment/default",
    "mage/url",
    'Magento_Checkout/js/model/quote',
    "Magento_Catalog/js/price-utils",
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Ui/js/model/messageList',
    'Magento_Checkout/js/action/set-billing-address'
    ],
    function (Component,url,quote,pu,additionalValidators,globalMessageList,setBillingAddressAction) {
        "use strict";

        return Component.extend({
            defaults: {
                template: "Scalapay_Scalapay/payment/payin4",
                redirectAfterPlaceOrder: false
            },

            /** Returns send check to info */
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            placeOrderSclapay: function (data, event) {
                if (event) {
                    event.preventDefault();
                }
                if (this.validate() &&
                    additionalValidators.validate() &&
                    this.isPlaceOrderActionAllowed() === true
                    ) {
                    setBillingAddressAction(globalMessageList);
                    var email = window.checkoutConfig.customerData.email;
                    if (!window.checkoutConfig.quoteData.customer_id) {
                        email = document.getElementById("customer-email").value;
                    }
                    email = encodeURIComponent(email);
                    setTimeout(function () {
                        window.location.replace(url.build("scalapay/index/index?email=" + email));
                    }, 4000);

                    return false;
                }
            },
            afterPlaceOrder: function (data, event) {

                //window.location.replace(url.build("scalapay/index/index"));

            },
            getScalapayTitle: function () {
                var grand_total = window.checkoutConfig.payment.grandtotal[this.item.method];
                //var number_of_ins = window.checkoutConfig.payment.scalapayconfig[this.item.method];
                var num = grand_total / 4;

                var new_price = pu.formatPrice(num);
                var str = window.checkoutConfig.payment.scalapay_title[this.item.method];
                var res = str.replace("[price]", new_price);

                return res;
            },
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            }

        });
    }
);
