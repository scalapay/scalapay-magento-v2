 // This function will contain all our code
function scalapayLibrary()
{
    var _scalapayObj = {};

       // This variable will be inaccessible to the user, only can be visible in the scope of your library.
       var config = {

            installmentTextProduct: 'oppure in 3 rate da soli <b>[price]</b> con [logo] <u>Vedi info ></u>',
            installmentTextCart: 'oppure in 3 rate da soli <b>[price]</b> con [logo] <u>Vedi info ></u>',
            installmentTextCheckout: '[logo] - 3 rate da soli [price]',
            eventName: null,
            eComPlatform: null,
            storeName: null,
            productName: null,
            productSku: null,
            currentPrice: null,
            installmentCopyDescriptionPlain: null,
            scalapayProductPrice: null,
            isInstock: null,
            isScalapayDisplayed: null,
            numberOfPayments: null,
            cartTotal: null,
            productInCart: null,
            checkoutTotal: null,
            paymentMethod: null,
            scalapayPopupContent: null


    };

    // Set default configurations
    _scalapayObj.setConfigVal = function (key,val) {

         config[key] = val;
         return key;
    };

    // Get default configurations
        _scalapayObj.getConfigVal = function (key) {

             return config[key];
        };

    // Replace product page scalapay placeholders
        _scalapayObj.installmentTextProductReplace = function (val,placholder) {

             var str = this.getConfigVal('installmentTextProduct');
             var res = str.replace(val,placholder);
             this.setConfigVal('installmentTextProduct',res);
        };


    // Replace cart page scalapay placeholders
        _scalapayObj.installmentTextCartReplace = function (val,placholder) {

             var str = this.getConfigVal('installmentTextCart');
             var res = str.replace(val,placholder);
             this.setConfigVal('installmentTextCart',res);
        };


    // Replace cart checkout scalapay placeholders
        _scalapayObj.installmentTextCheckoutReplace = function (val,placholder) {

             var str = this.getConfigVal('installmentTextCheckout');
             var res = str.replace(val,placholder);
             this.setConfigVal('installmentTextCheckout',res);
        };

    //Magento 2.x fix for configurable products
    //intilize popup on product view page
        _scalapayObj.intPopUp = function () {

            var modal = document.querySelector(".scalapayModal");
            var closeButton = document.querySelector(".scalapay-close-button");

            function closePopup()
            {
                modal.classList.remove("scalapay-show-modal");
            }

            function windowOnClick(event)
            {
                if (event.target === modal) {
                    closePopup();
                }
            }

            closeButton.addEventListener("click", closePopup);
            closeButton.addEventListener("touchstart", closePopup);
            window.addEventListener("click", windowOnClick);
            window.addEventListener("touchstart", windowOnClick);
        }

    //Magento 2.x fix for configurable products
    //intilize popup on product view page
        _scalapayObj.showPopup = function (url) {
            if (url != "") {
                document.getElementById("process-scalapay").style.display = 'block';
                var modal = document.querySelector(".scalapayModal");

                if (modal.classList.contains("scalapay-show-modal")) {
                    //showing
                    modal.classList.remove("scalapay-show-modal");
                } else {
                    if (config.scalapayPopupContent != null) {
                        document.getElementById('popupContents').innerHTML = config.scalapayPopupContent;
                        setTimeout(function () {
                            modal.classList.add("scalapay-show-modal");
                            document.getElementById("process-scalapay").style.display = 'none';
                        }, 700);
                    } else {
                        //hidden
                        var xmlhttp;

                        if (window.XMLHttpRequest) {
                           // code for modern browsers
                            xmlhttp = new XMLHttpRequest();
                        } else {
                       // code for old IE browsers
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }

                        xmlhttp.onreadystatechange = function () {
                            if (this.readyState == 4 && this.status == 200) {
                                config.scalapayPopupContent = this.responseText;
                                document.getElementById('popupContents').innerHTML = config.scalapayPopupContent;
                                setTimeout(function () {
                                    modal.classList.add("scalapay-show-modal");
                                    document.getElementById("process-scalapay").style.display = 'none';
                                }, 700);
                            } else if ([404, 500 , 503, 504 ].indexOf(this.status) > -1 ) {
                                document.getElementById("process-scalapay").style.display = 'none';
                                return;
                            }
                        };

                        xmlhttp.onerror = function (e) {
                            document.getElementById("process-scalapay").style.display = 'none';
                            return;
                        };

                        try {
                            xmlhttp.open("GET", url, true);
                            xmlhttp.send();
                        } catch (e) {
                            return;
                        }
                    }
                }
            }

        }

    //intilize scalapay product text
        _scalapayObj.intScalapayProductText = function (elemSelector,divToShow) {
            var referenceNode = document.querySelector(elemSelector);
            var newNode = document.getElementById(divToShow);
            referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
        }

    //intilize scalapay cart text before
        _scalapayObj.intScalapayCartText = function (source, destination) {

            var sourceNode = document.querySelector(source);
            var destinationNode = document.getElementById(destination);
            sourceNode.parentNode.insertBefore(destinationNode, sourceNode);
        }

        return _scalapayObj;
}

var windowScalapay = null;
// We need that our library is globally accesible, then we save in the window
if (typeof(windowScalapay) === 'undefined' || windowScalapay == null) {
    windowScalapay = scalapayLibrary();
}