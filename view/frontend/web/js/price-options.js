define([
    'jquery',
    'underscore',
    'mage/template',
    'priceUtils',
    'priceBox',
    'priceOptions'
], function ($, _, mageTemplate, utils) {
    'use strict';
    return function (priceOptions) {

        return $.widget('mage.priceOptions', priceOptions, {

            _onOptionChanged: function onOptionChanged(event)
            {
                alert("cccc");
                console.log('TEST!');
                var changes,
                    option = $(event.target),
                    handler = this.options.optionHandlers[option.data('role')];
                option.data('optionContainer', option.closest(this.options.controlContainer));

                if (handler && handler instanceof Function) {
                    changes = handler(option, this.options.optionConfig, this);
                } else {
                    changes = defaultGetOptionValue(option, this.options.optionConfig);
                }
                $(this.options.priceHolderSelector).trigger('updatePrice', changes);

                var no_of_payments = $("#no_of_payments").text();
                var current_price = $("span.price").text();
                var res = current_price.replace("$","");

                var new_price = 0;
                if (no_of_payments) {
                    new_price = (res / no_of_payments).toFixed(2);
                }

                $("#installment_text").text("oppure solo " + new_price + " in  tre rate  mensili con Scalapay");


            }

        });
    }
});
