<?php

/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Scalapay\Scalapay\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class CurrencyPosition implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 'after', 'label' => 'After'],
            ['value' => 'before', 'label' => 'Before']
        ];
    }
}
