<?php

/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class CurrencyDisplay implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 'symbol', 'label' => 'Symbol'],
            ['value' => 'code', 'label' => 'Code']
        ];
    }
}
