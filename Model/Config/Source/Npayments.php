<?php

namespace Scalapay\Scalapay\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Npayments implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 3, 'label' => '3'],
            ['value' => 4, 'label' => '4']
        ];
    }
}
