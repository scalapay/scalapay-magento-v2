<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Scalapay\Scalapay\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class Allspecificlanguages implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('All Allowed Languages')],
            ['value' => 1, 'label' => __('Specific Languages')]
        ];
    }
}
