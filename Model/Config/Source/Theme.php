<?php

/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Scalapay\Scalapay\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Theme implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 'primary', 'label' => 'Primary'],
            ['value' => 'variant', 'label' => 'Variant']
        ];
    }
}
