<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Scalapay\Scalapay\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\OfflinePayments\Model\Banktransfer;
use Magento\OfflinePayments\Model\Cashondelivery;

class InstructionsConfigProvider implements ConfigProviderInterface
{
    /**
     * @var string[]
     */
    protected $methodCodes = [
        'scalapay',
        'payin4',
        'paylater',
        Banktransfer::PAYMENT_METHOD_BANKTRANSFER_CODE,
        Cashondelivery::PAYMENT_METHOD_CASHONDELIVERY_CODE
    ];

    /**
     * @var \Magento\Payment\Model\Method\AbstractMethod[]
     */
    protected $methods = [];

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * @param PaymentHelper $paymentHelper
     * @param Escaper $escaper
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        Escaper $escaper
    ) {
        $this->escaper = $escaper;
        foreach ($this->methodCodes as $code) {
            $this->methods[$code] = $paymentHelper->getMethodInstance($code);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [];
        foreach ($this->methodCodes as $code) {
            if ($code == 'scalapay' || $code == 'payin4' || $code == 'paylater') {
                $config['payment']['instructions'][$code] = $this->getInstructions($code);
                $config['payment']['scalapayconfig'][$code] = $this->getScalapayconfig($code);
                $config['payment']['scalapay_title'][$code] = $this->getScalapayTitle($code);
                $config['payment']['grandtotal'][$code] = $this->getGrandTotal($code);
            } else {
                $config['payment']['instructions'][$code] = $this->getInstructions($code);
            }
        }

        return $config;
    }

    /**
     * Get instructions text from config
     *
     * @param string $code
     * @return string
     */
    protected function getInstructions($code)
    {
        return $this->methods[$code]->getInstructions();
    }

    /**
     * Get instructions text from config
     *
     * @param string $code
     * @return string
     */
    protected function getScalapayTitle($code)
    {
        return $this->methods[$code]->getScalapayTitle();
    }

    /**
     * Get scalapayConfig text from config
     *
     * @param string $code
     * @return string
     */
    protected function getScalapayconfig($code)
    {
        return $this->methods[$code]->getScalapayconfig();
    }

    /**
     * Get Cart GrandTotal Including Tax
     *
     * @return float
     */
    protected function getGrandTotal($code)
    {
        return $this->methods[$code]->getGrandTotal();
    }
}
