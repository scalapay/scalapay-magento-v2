<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Scalapay\Scalapay\Model;

use Magento\Sales\Model\Order;

/**
 * Pay In Store payment method model
 */
class Scalapay extends \Magento\Payment\Model\Method\AbstractMethod
{
    protected $_code = 'scalapay';
    protected $_isOffline = false;
    protected $_isGateway = false;
    protected $_canCapture = true;
    protected $_canAuthorize = false;
    protected $_canCapturePartial = true;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid = false;
    protected $_canFetchTransactionInfo = true;
    protected $_helper;
    protected $_scopeConfig;
    protected $_session;
    protected $_state;
    protected $_isInitializeNeeded = true;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Scalapay\Scalapay\Helper\Data $helper
     * @param \Magento\Checkout\Model\Session $session
     * @param \Magento\Framework\App\State $state
     * @param array $data
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Scalapay\Scalapay\Helper\Data $helper,
        \Magento\Checkout\Model\Cart $session,
        \Magento\Framework\App\State $state,
        array $data = [],
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );

        $this->_helper = $helper;
        $this->_session = $session;
        $this->_state = $state;
    }

    /**
     * Capture payment abstract method
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @deprecated 100.2.0
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->_helper->log("Capture call Internal");

        $order = $payment->getOrder();
        $orderToken = $order->getOrderToken();
        $payment
                ->setTransactionId($orderToken)
                ->setIsTransactionClosed(0);

        return $this;
    }

    /**
     * Refund specified amount for payment
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @deprecated 100.2.0
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->_helper->log("Refund Internal");
        if ($this->canRefund()) {
            $creditmemo = $payment->getCreditmemo();
            $order = $payment->getOrder();
            $orderToken = $order->getOrderToken();
            $currency = $order->getOrderCurrencyCode();
            $merchantOrder = $order->getIncrementId();
            $paymentMethod = $payment->getMethodInstance()->getCode();
            $this->_helper->log("paymentMethodName: " . $paymentMethod);
            $this->_helper->log("order incrementId: " . $order->getIncrementId());
            $this->_helper->log("orderToken: " . $orderToken);

            if ($paymentMethod == "scalapay") {
                if (isset($orderToken) && $orderToken != "") {
                    $paymentInfo = $this->_helper->scalapayPaymentStatus($orderToken);
                    $this->_helper->log("scalapayPaymentStatus: " . $paymentInfo["status"]);
                    if (!$paymentInfo["error"]) {
                        if (
                            (
                                $paymentInfo["status"] == "charged" ||
                                $paymentInfo["status"] == "partially_refunded"
                            ) &&
                            $amount > 0 && $paymentInfo["total_amount"]["amount"] > 0
                        ) {
                            $refundInfo = $this->_helper->scalapayRefundPayment(
                                $orderToken,
                                $merchantOrder,
                                $amount,
                                $currency
                            );
                            if (isset($refundInfo['refundToken'])) {
                                $this->_helper->log("Refunded.");
                                $this->_helper->log(
                                    $amount . $currency . "\n Refunded Token: " . $refundInfo['refundToken']
                                );
                                $creditmemo->setTransactionId($refundInfo['refundToken']);
                                $creditmemo->save();
                            } elseif (isset($refundInfo['errorCode'])) { //errorCode: pre_condition_failed
                                $this->_helper->log("Not Refunded - there is some error.");
                                $this->_helper->log($amount . $currency);
                                $this->_helper->log(
                                    "error Id: " . $refundInfo['errorId'] . "\n message: " . $refundInfo['message']
                                );
                                throw new \Magento\Framework\Exception\LocalizedException(__($refundInfo['message']));
                            }
                        } elseif ($paymentInfo["status"] == "refunded") {
                            $this->_helper->log("Not Refunded.");
                            $this->_helper->log("Already refunded");
                            $this->_helper->log($paymentInfo["order_details"]);
                            throw new \Magento\Framework\Exception\LocalizedException(__('Already refunded'));
                        } else {
                            $this->_helper->log("Not Refunded.");
                            $this->_helper->log("Error");
                            $this->_helper->log($paymentInfo["order_details"]);
                            throw new \Magento\Framework\Exception\LocalizedException(__('Not Refunded'));
                        }
                    } else {
                        $this->_helper->log("Not Refunded.");
                        $this->_helper->log($paymentInfo['error']);
                        $this->_helper->log($paymentInfo['message']);
                        throw new \Magento\Framework\Exception\LocalizedException(__($paymentInfo['message']));
                    }
                }//order token
            }// paymentmethod
        }
        return $this;
    }

    public function getInstructions()
    {
        return $this->_helper->getScalapayCheckoutInstallmentDescription();
    }

    public function isInitializeNeeded()
    {
        return $this->_isInitializeNeeded;
    }

    public function getScalapayTitle()
    {
        $configData = $this->_helper->getScalapayConfig();
        $numberOfPayments = $configData["numberOfPayments"];
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $getCurrentQuote = $this->_session->getQuote();
        $grandTotal = $getCurrentQuote->getGrandTotal();
        $price = round($grandTotal / $numberOfPayments, 2);
        $installment_checkout_description = $this->_helper->getScalapayCheckoutPageText($price);

        return $installment_checkout_description;
    }

    public function getScalapayconfig()
    {
        $configData = $this->_helper->getScalapayConfig();
        $numberOfPayments = $configData["numberOfPayments"];

        return $numberOfPayments;
    }

    /**
  * Check whether payment method is applicable to quote
  * Purposed to allow use in controllers some logic that was implemented in blocks only before
  *
  * @param \Magento\Quote\Api\Data\CartInterface|null $quote
  * @return bool
  */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        $isEnabled = $this->_helper->getConfigData('active');
        $getCurrentQuote = $this->_session->getQuote();
        if (
            $getCurrentQuote->getGrandTotal() == 0 &&
            (
                $this->_state->getAreaCode() == 'webapi_rest' ||
                $this->_state->getAreaCode() == 'webapi_soap'
            )
        ) {
            $getCurrentQuote = $quote;
        }
        $grandTotal = $getCurrentQuote->getGrandTotal();
        $currentCountry = $getCurrentQuote->getShippingAddress()->getCountryId();
        $getAllItems = $getCurrentQuote->getAllItems();
        $productIds = [];
        foreach ($getAllItems as $item) {
            if (
                $item->getProductType() == 'configurable' ||
                $item->getProductType() == 'grouped' ||
                $item->getProductType() == 'bundle'
            ) {
                continue;
            } else {
                $productIds[] = $item->getProductId();
            }
        }

        //Show scalapay payment method if enabled
        //Do not show Sclapay method if price range does not meet the criteria
        //Do not show scalapay payment method if we any virtual product in cart
        //OR if a product exist under disabled categories
        $isActiveScalapay = $this->_helper->isScalapayActive($grandTotal, $productIds, $currentCountry);

        if ($isActiveScalapay) {
            return true;
        } else {
            return false;
        }
    }

    public function getGrandTotal()
    {
        $getCurrentQuote = $this->_session->getQuote();
        $grandTotal = $getCurrentQuote->getGrandTotal();

        return $grandTotal;
    }

    public function initialize($paymentAction, $stateObject)
    {
        $stateObject->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
        $stateObject->setStatus('pending_payment');
    }
}
