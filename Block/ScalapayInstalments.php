<?php

namespace Scalapay\Scalapay\Block;

use Scalapay\Scalapay\Helper\Data as HelperData;

class ScalapayInstalments extends \Magento\Framework\View\Element\Template
{
    protected $_registry;
    protected $_productFactory;
    protected $_helper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     * @param HelperData $helperData
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        HelperData $helperData,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = []
    ) {
        $this->_registry = $registry;
        $this->_productFactory = $productFactory;
        $this->_helper = $helperData;
        parent::__construct($context, $data);
    }

    /**
     * Get Current Product
     *
     * @return object
     *
     */
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * Get Helper
     *
     * @return object \Scalapay\Scalapay\Helper\Data
     *
     */
    public function getHelper()
    {
        return $this->_helper;
    }
}
