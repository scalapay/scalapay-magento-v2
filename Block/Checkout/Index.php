<?php

namespace Scalapay\Scalapay\Block\Checkout;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_cart;
    protected $_helper;

    /**
    *  \Magento\Framework\View\Element\Template\Context $context
    *  \Magento\Checkout\Model\Cart $cart
    *  \Scalapay\Scalapay\Helper\Data $helper
    *  array $data
    */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Scalapay\Scalapay\Helper\Data $helper,
        array $data = []
    ) {
        $this->_cart = $cart;
        $this->_helper = $helper;

        parent::__construct($context, $data);
    }

    /**
     * show Message
     *
     * @return string
     */
    public function showCustomMessage()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $coreSession = $objectManager->get('\Magento\Framework\Session\SessionManagerInterface');
        $coreSession->start();
        $message_to_show = $coreSession->getMessage();
        if (isset($message_to_show) && $message_to_show != "") {
            $messageHtml = '<div class="page messages">
            <div>
                <div class="messages">
                    <div class="message-notice notice message" >
                        <div>' . __($message_to_show) . '</div>
                    </div>
                </div>
            </div>
            </div>';

            return $messageHtml;
        }

        $coreSession->unsMessage();
    }

    /**
     * Return the current cart
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getCart()
    {
        return $this->_cart;
    }

    public function getCartProductsData()
    {
        $getCurrentQuote = $this->getCart()->getQuote();
        $getAllitems = $getCurrentQuote->getAllItems();
        $product_data = "";

        if (is_array($getAllitems)) {
            foreach ($getAllitems as $item) {
                $id = $item->getProductId();
                $name = $item->getName();
                $sku = $item->getSku();
                $qty = $item->getQty();
                $price = $item->getPrice();

                $product_data .= "{
                    product_id: '" . $id . "',
                    sku: '" . $sku . "',
                    name: '" . $name . "',
                    price: " . $price . ",
                    quantity: " . $qty . "
                },";
            }

            $product_data = $this->_helper->crtrim($product_data, ',');
        }

        return $product_data;
    }

    /**
     * Get Helper
     *
     * @return object \Scalapay\Scalapay\Helper\Data
     *
     */
    public function getHelper()
    {
        return $this->_helper;
    }
}
