<?php

namespace Scalapay\Scalapay\Block\Adminhtml\System\Config\Button;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Scalapay\Scalapay\Helper\Data as HelperData;

class GetApiConfiguration extends Field
{
    protected $helper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        HelperData $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('system/config/get_api_configuration.phtml');
        }

        return $this;
    }

    public function getScalapayConfiguration()
    {
        return $this->helper->getScalapayConfigV3();
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
