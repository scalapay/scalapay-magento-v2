<?php

namespace Scalapay\Scalapay\Block\Adminhtml\System\Config\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

class Disabled extends Field
{
    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setDisabled('disabled');
        $element->setReadonly('readonly');
        return $element->getElementHtml();
    }
}
