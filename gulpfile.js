const gulp = require('gulp');
const jsonModify = require('gulp-json-modify');
const ver = require('./package.json').version;
const  parser = require('xml2json');
const fs = require('fs');
function upversion(done)
{
    console.log('current version: ', ver);
    const splitString = ver.split('.', 3);
    const patchVersion = splitString[2].split('"', 1);
    let patchNumber = Number(patchVersion[0]);
    patchNumber++;
    splitString[2] = String(patchNumber);
    process.env.VERSION = splitString.join('.');
    console.log('new version: ', process.env.VERSION);
    done();
}

function upminversion(done)
{
    console.log('current version: ', ver);
    const splitString = ver.split('.', 3);
    const patchVersion = splitString[1].split('"', 1);
    let patchNumber = Number(patchVersion[0]);
    patchNumber++;
    splitString[1] = String(patchNumber);
    process.env.VERSION = splitString.join('.');
    console.log('new version: ', process.env.VERSION);
    done();
}


function upmajversion(done)
{
    console.log('current version: ', ver);
    const splitString = ver.split('.', 3);
    const patchVersion = splitString[0].split('"', 1);
    let patchNumber = Number(patchVersion[0]);
    patchNumber++;
    splitString[0] = String(patchNumber);
    process.env.VERSION = splitString.join('.');
    console.log('new version: ', process.env.VERSION);
    done();
}

function saveversion()
{
    console.log('updating package version: ', process.env.VERSION);
    console.log('===== updating module.xml =====');
    fs.readFile('./etc/module.xml', function (err, data) {
        var json = JSON.parse(parser.toJson(data, {reversible: true}));
        console.log(json['config']['module']['setup_version']);
        json['config']['module']['setup_version'] = process.env.VERSION;
        var stringified = JSON.stringify(json);
        var xml = parser.toXml(stringified);

        var xml2 = '<?xml version="1.0"?>' + xml;

        fs.writeFile('./etc/module.xml', xml2, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                console.log('module.xml updated!');
            }
        });
    });
    console.log('===== updating composer.json =====');
    gulp.src(['./composer.json'])
    .pipe(jsonModify({
        key: 'version',
        value: process.env.VERSION
    }))
    .pipe(gulp.dest('./'));
    console.log('===== updating package.json =====');
    return gulp.src(['./package.json'])
    .pipe(jsonModify({
        key: 'version',
        value: process.env.VERSION
    }))
    .pipe(gulp.dest('./'));
}

const versions = gulp.series(upversion, saveversion);

gulp.task('autoversion', (done) => {
    console.log('===== Auto Version =====');
    versions();
    done();
});


const minversions = gulp.series(upminversion, saveversion);

gulp.task('minversion', (done) => {
    console.log('===== Minor Version =====');
    minversions();
    done();
});


const majversions = gulp.series(upmajversion, saveversion);

gulp.task('majversion', (done) => {
    console.log('===== Major Version =====');
    majversions();
    done();
});