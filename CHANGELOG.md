# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [2.2.92] - 2023-05-16
### Fixed
- IN-3796 - Fixed compatibility issues with php8.

## [2.2.91] - 2023-05-02
### Fixed
- IN-3796 - Fixed payment method issue.

## [2.2.90] - 2023-04-27
### Fixed
- SP-9472 - Solves pipeline issue.

## [2.2.89] - 2023-03-31
### Removed
- Removed checks for product qty and street address.

## [2.2.88] - 2023-03-30
### Added
- Added checks for product qty and street address.

## [2.2.87] - 2023-01-18
### Added
- Added Scalapay widget in checkout description.

## [2.2.86] - 2023-01-06
### Fixed
- Fixed Deprecated Functionality: trim()

## [2.2.85] - 2023-01-06
### Fixed
- Fixed Notice - Undefined property _helper.

## [2.2.84] - 2022-12-20
### Changed
- Changed Scalapay logo size in checkout widget.

## [2.2.83] - 2022-12-19
### Changed
- Changed Scalapay logo size in checkout widget.

## [2.2.82] - 2022-12-16
### Removed
- Removed unwanted comments.

## [2.2.79] - 2022-11-16
### Fixed
- Fixed pay in 3, pay in 4 and pay later limit checks.

## [2.2.78] - 2022-11-02
### Fixed
- Fixed eror in repo version

## [2.2.77] - 2022-11-02
### Added
- Added feature V3 Scalapay configuration.
- Added PT translations.

### Fixed
- Fixed invalid payment method issue.
- Fixed css issue on checkout page.

## [2.2.76] - 2022-10-11
### Removed
-  Removed stock checks.

## [2.2.75] - 2022-10-10
### Added
-  Added stock checks.

## [2.2.74] - 2022-09-16
### Removed
-  Removed stock checks.

### Added
-  Applied PSR and Lint on code.

## [2.2.76] - 2022-10-10
### Removed
- Removed stock checks.

### Added
- Added block name in layout.

## [2.2.75] - 2022-10-10
### Added
- Added stock checks.
- Added new logs and new catch

## [2.2.74] - 2022-09-16
### Removed
- removed stock checks.
- Applied PSR and Lint on code

## [2.2.73] - 2022-09-07
### Added
- Added stock checks.

## [2.2.72] - 2022-08-13
### Removed
- Removed stock checks.

## [2.2.71] - 2022-08-12
### Added
- Added transaction code and added stock checks.

## [2.2.67] - 2022-08-08
### Removed
- Removed Stock check.

## [2.2.66] - 2022-08-08
### Added
- Added Stock check.

### Fixed
- Fixed data issue used trim, and fixed pay4later and pay in 3 and 4 issues.

## [2.2.65] - 2022-08-05
### Added
-  New payment options , pay in 4 and pay later.

## [2.2.62] - 2022-02-08
### Added
-  Added number of payments to 4.

## [2.2.61] - 2022-02-04
### Added
-  Added Spanish language support

## [2.2.60] - 2022-02-01
### Added
-  Customized value in plugin details

## [2.2.58] - 2022-01-05
### Changed
- Changed staging endpoint.

## [2.2.57] - 2021-12-30
### Fixed
- Fixed curl, log and undefined index issues.

## [2.2.56] - 2021-10-19
### Added
- Added text below widget on product page.

## [2.2.55] - 2021-05-10
### Changed
- Changed Scalapay text on checkout page.

## [2.2.54] - 2021-04-04
### Added
- Added CHANGELOG.md file.

## [2.2.53] - 2021-02-09
### Added
- Added check if category does not exist.

## [2.2.52] - 2021-01-28
### Added
- Applied epsilon calculation on grandtotal compare

## [2.2.51] - 2021-01-28
### Changed
- Changed Scalapay refund orderToken index in response

## [2.2.50] - 2021-01-25
### Fixed
- Fixed Scalapay capture call params

## [2.2.49] - 2021-01-25
### Fixed
- Fixed Discount and order total issue

## [2.2.48] - 2021-01-24
### Added
- Applied checks to calculate order total and dicount

### Fixed
- Fixed discount and order total issue

## [2.2.47] - 2021-01-24
### Removed
- Removed unwanted logs

## [2.2.46] - 2021-01-21
### Changed
- Initialize order status as pending payment

## [2.2.45] - 2020-12-21
### Fixed
- Fixed email issue on convert quote.

## [2.2.44] - 2020-12-18
### Changed
- Update composer php version

## [2.2.43] - 2020-12-17
### Added
- prepare for composer

## [2.2.42] - 2020-12-14
### Fixed
- Address problem fix

## [2.2.41] - 2020-11-17
### Added
- Validate address information before redirect

## [2.2.40] - 2020-11-13
### Added
- Added scalapay and magento version in payload

### Fixed
- fixed special char Email issue.

## [2.2.39] - 2020-11-03
### Added
- Added log on cacnel order.

## [2.2.38] - 2020-11-03
### Added
- Added region and Order status checks

### Fixed
- Fixed order status and address issue.
- Fixed billing address save issue.
- Fixed order status issue while creating before order.

### Removed
- removed checks Order status

## [2.2.37] - 2020-10-22
### Changed
- Moved capture call before invoice.

### Added
- Added catches for Exception.

## [2.2.36] - 2020-10-16
### Changed
- Changed mode label

## [2.2.35] - 2020-10-16
### Changed
- Changed label

## [2.2.34] - 2020-10-14
### Added
- write errors and exception in logs.

### Fixed
- Fixed undefined var error.
- Fixed xsd references in xml files.

### Removed
- Deleted extra files - classes.
- Remove unused variables, files and code.

## [2.2.33] - 2020-10-01
### Fixed
- Fixed error handling in payment process

## [2.2.32] - 2020-09-30
### Fixed
- Fixed stock issue.

## [2.2.31] - 2020-09-16
### Removed
- Remove checkout config fields from admin.

## [2.2.30] - 2020-09-04
### Fixed
- Fixed terms and condition chkbox issue.

## [2.2.29] - 2020-09-03
### Fixed
- bugfix/bauzaar-stock-issue-on-confirm-page Fixed stock issue on confirm page.

## [2.2.28] - 2020-08-20
### Added
- Readonly endpoint urls

### Changed
- Translations updated
- Production and test info separated.

### Fixed
- Fixed error if api password is wrong.

### Removed
- Removed translations for admin site configuration.

## [2.2.27] - 2020-08-12
### Fixed
- Fixed currency save error in configuration.

## [2.2.26] - 2020-07-28
### Changed
- Checkout logo changed.

## [2.2.25] - 2020-07-28
### Fixed
- Fixed billing address, category and product errors.

## [2.2.24] - 2020-07-21
### Fixed
- Fixed mini cart issue after order place.

## [2.2.23] - 2020-07-17
### Fixed
- Fixed onestepcheckout scalapay issues
- Set the default title with price tag

## [2.2.22] - 2020-07-16
### Removed
- Removed merchant refference.

## [2.2.21] - 2020-07-16
### Removed
- Removed merchant refference.

### Changed
- Bauzaar specific changes
- bauzaar scalapay integration ma

### Fixed
- Fixed tag IT translation

## [2.2.20] - 2020-07-16
### Fixed
- Fixed - checkout page - Empty Scalapay title

## [2.2.19] - 2020-07-09
### Added
- add option to show hide price of

## [2.2.18] - 2020-07-09
### Fixed
- fixed price issue on product and cart page

### Added
- Feature Added - Show/Hide price in widget

## [1.1.17] - 2020-06-16
### Fixed
- Fixed guest checkout issue during order

## [1.1.16] - 2020-06-16
### Fixed
- Fixed address issue and payment method list in API

## [1.1.15] - 2020-06-02
### Added
- Added payload - category, subcategory, brand

## [1.1.14] - 2020-05-29
### Fixed
- Added try/catch on quote convert to orders.
- Fixed validation and Changed session class

## [1.1.13] - 2020-05-20
### Fixed
- clear session if order is canceled

## [1.1.12] - 2020-05-20
### Fixed
- Fixed by cancel order if by pass scalapay + fixed variable issue on checkout

## [1.1.11] - 2020-05-15
### Fixed
- Fixed shipping address name in before v1 call

## [1.1.10] - 2020-05-13
### Added
- magento v2 adminhtml.php for permission

## [1.1.9] - 2020-04-17
### Added
- Added ACL rules for scalapay settings

### Changed
- Changed plugin UI
- update scalapay from .it to .com

### Fixed
- Fixed system fields title
- Fixed function name
- Fixed category bug
- fixed message translation
- Fixed module conflict issue

## [1.1.8] - 2020-04-17
### Removed
- removed checks on virtal prodcts and order total in Payment is Active method

## [1.1.7] - 2020-04-17
### Added
- Feature Added - Disable scalapay for specific categories.

### Fixed
- Fixed- Disabled category check also include sub-category

## [1.1.6] - 2020-04-13
### Fixed
- Fixed German translation.

## [1.1.5] - 2020-04-10
### Fixed
- fixed translations text.
- Fixed readonly fields issue + restrict empty field on scalapay order.

## [0.0.4] - 2020-04-10
### Changed
- separate logo for italy

## [0.0.3] - 2020-04-10
### Changed
- update widget translations

## [0.0.2] - 2020-04-10
### Added
- Scalapay widget and removed v1 calls
- feature added. separate logo for italy.

### Changed
- Widget translation updated.

### Fixed
- Fixed product and cart configuration labels
- Fixed translations and scalapay class selectors.

## [0.0.1] - 2020-04-01
### Added
- Initial commit for M2 Marketplace
- Added duplicate order check.
- Added refund feature
- Added scalapay installment description
- Feature added: Segment not installed by default
- Added seprate text on cart and product page
- feature added: Multi-language
- Stop snding invoice email and other corrections.
- Assign default values to scalapay configuration values.
- Feature added - Multilingual popup.
- Added dynamic languages check in multilingual popup.
- Added latest french translation.
- Magento V2- Add versioning to the bitbucket pipeline

### Changed
- changed in call url v1 to v2 and added api

### Removed
- Segments removed

### Fixed
- Fixed duplicated translation bug
- Fixed issue - on create order when there is last item in stock.
- Fixed issue -when scalapay server is unavailable.
- Fixed version and new popup
- fixed stock item issue on order creation.
