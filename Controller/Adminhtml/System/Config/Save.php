<?php

namespace Scalapay\Scalapay\Controller\Adminhtml\System\Config;

use Magento\Config\Controller\Adminhtml\System\Config;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Config\Controller\Adminhtml\System\AbstractConfig;
use Scalapay\Scalapay\Helper\Data as HelperData;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;

/**
 * System Configuration Save Controller
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save extends \Magento\Config\Controller\Adminhtml\System\Config\Save
{
    protected $configWriter;
    protected $storeManager;
    protected $helper;
    protected $cacheTypeList;
    protected $cacheFrontendPool;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Config\Model\Config\Structure $configStructure
     * @param \Magento\Config\Controller\Adminhtml\System\ConfigSectionChecker $sectionChecker
     * @param \Magento\Config\Model\Config\Factory $configFactory
     * @param \Magento\Framework\Cache\FrontendInterface $cache
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Config\Model\Config\Structure $configStructure,
        \Magento\Config\Controller\Adminhtml\System\ConfigSectionChecker $sectionChecker,
        \Magento\Config\Model\Config\Factory $configFactory,
        \Magento\Framework\Cache\FrontendInterface $cache,
        \Magento\Framework\Stdlib\StringUtils $string,
        HelperData $helper,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool
    ) {
        parent::__construct($context, $configStructure, $sectionChecker, $configFactory, $cache, $string);
        $this->helper = $helper;
        $this->configWriter = $configWriter;
        $this->storeManager = $storeManager;
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * Advanced save procedure
     *
     * @return bool
     */
    protected function saveScalapayConfiguration()
    {
        $this->helper->log("validating Scalapay config before save.");
        $sp = $this->getRequest()->getParams();
        $payin3Max = isset($sp['groups']['payin3']['groups']['restrictions']['fields']['max_amount']['value']) ?
        $sp['groups']['payin3']['groups']['restrictions']['fields']['max_amount']['value'] : null;
        $payin3Min = isset($sp['groups']['payin3']['groups']['restrictions']['fields']['min_amount']['value']) ?
        $sp['groups']['payin3']['groups']['restrictions']['fields']['min_amount']['value'] : null;
        $payin4Max = isset($sp['groups']['payin4']['groups']['restrictions']['fields']['max_amount']['value']) ?
        $sp['groups']['payin4']['groups']['restrictions']['fields']['max_amount']['value'] : null;
        $payin4Min = isset($sp['groups']['payin4']['groups']['restrictions']['fields']['min_amount']['value']) ?
        $sp['groups']['payin4']['groups']['restrictions']['fields']['min_amount']['value'] : null;
        $paylaterMax = isset($sp['groups']['paylater']['groups']['restrictions']['fields']['max_amount']['value']) ?
        $sp['groups']['paylater']['groups']['restrictions']['fields']['max_amount']['value'] : null;
        $paylaterMin = isset($sp['groups']['paylater']['groups']['restrictions']['fields']['min_amount']['value']) ?
        $sp['groups']['paylater']['groups']['restrictions']['fields']['min_amount']['value'] : null;


        $this->helper->log("payin3 max: " . $payin3Max);
        $this->helper->log("payin3Min: " . $payin3Min);
        $this->helper->log("payin4Max: " . $payin4Max);
        $this->helper->log("payin4Min: " . $payin4Min);
        $this->helper->log("paylaterMax: " . $paylaterMax);
        $this->helper->log("paylaterMin: " . $paylaterMin);

        $config = $this->helper->getScalapayConfigV3();

        $message = [];
        $isError = false;
        $errorPayin3Min = false;
        $errorPayin3Max = false;
        $errorPayin3Init = null;

        if (
            !isset($config['payin3']["maximumAmount"]) &&
            $this->helper->getConfigData('active')
        ) {
            $this->messageManager->addError(
                __("Scalapay - Pay in 3 is not available.")
            );
            $this->configWriter->save('payment/scalapay/active', 0);
        }
        if (
            !isset($config['payin4']["maximumAmount"]) &&
            $this->helper->getConfigDataPayin4('active')
        ) {
            $this->messageManager->addError(
                __("Scalapay - Pay in 4 is not available.")
            );
            $this->configWriter->save('payment/payin4/active', 0);
        }
        if (
            !isset($config['paylater']["maximumAmount"]) &&
            $this->helper->getConfigDataPayLater('active')
        ) {
            $this->messageManager->addError(
                __("Scalapay - Pay later is not available.")
            );
            $this->configWriter->save('payment/paylater/active', 0);
        }

        if (!isset($config['payin3']["minimumAmount"])) {
            $config['payin3']["minimumAmount"] = 5;
        }
        if (!isset($config['payin3']["maximumAmount"])) {
            $config['payin3']["maximumAmount"] = 899;
        }
        if (!isset($config['payin4']["minimumAmount"])) {
            $config['payin4']["minimumAmount"] = 900;
        }
        if (!isset($config['payin4']["maximumAmount"])) {
            $config['payin4']["maximumAmount"] = 1500;
        }
        if (!isset($config['paylater']["minimumAmount"])) {
            $config['paylater']["minimumAmount"] = 5;
        }
        if (!isset($config['paylater']["maximumAmount"])) {
            $config['paylater']["maximumAmount"] = 1500;
        }
        if (
            isset($config['payin3']["minimumAmount"]) &&
            isset($payin3Min) &&
            isset($config['payin3']["maximumAmount"]) &&
            isset($payin3Max)
        ) {
            $errorPayin3Init = __(
                "Scalapay - pay in 3 amount should be between %1 and %2.",
                $config['payin3']["minimumAmount"],
                $config['payin3']["maximumAmount"]
            );
            if ($config['payin3']["minimumAmount"] > $payin3Min) {
                $errorPayin3Min = true;
                $isError = true;
            } elseif ($payin3Min > $config['payin3']["maximumAmount"]) {
                $errorPayin3Min = true;
                $isError = true;
            } elseif (
                $payin3Min > $payin3Max &&
                $payin3Max <= $config['payin3']["maximumAmount"]
            ) {
                $errorPayin3Min = true;
                $isError = true;
            }

            //max value
            if ($config['payin3']["maximumAmount"] < $payin3Max) {
                $errorPayin3Max = true;
                $this->configWriter->save(
                    'scalapay/payin3/restrictions/max_amount',
                    $config['payin3']["maximumAmount"]
                );
                $isError = true;
            } elseif ($payin3Max < $config['payin3']["minimumAmount"]) {
                $errorPayin3Max = true;
                $isError = true;
            } elseif (
                $payin3Max < $payin3Min &&
                $payin3Min >= $config['payin3']["minimumAmount"]
            ) {
                $errorPayin3Max = true;
                $isError = true;
            }
            if ($isError == true) {
                if ($errorPayin3Min == true || $errorPayin3Max == true) {
                    $this->messageManager->addError($errorPayin3Init);
                }
            }
        }

        ////pay in 4
        $errorPayin4Min = false;
        $errorPayin4Max = false;
        $errorPayin4Init = null;
        if (
            isset($config['payin4']["minimumAmount"]) &&
            isset($payin4Min) &&
            isset($config['payin4']["maximumAmount"]) &&
            isset($payin4Max)
        ) {
            $errorPayin4Init = __(
                "Scalapay - pay in 4 amount should be between %1 and %2.",
                $config['payin4']["minimumAmount"],
                $config['payin4']["maximumAmount"]
            );
            if ($config['payin4']["minimumAmount"] > $payin4Min) {
                $errorPayin4Min = true;
                $isError = true;
            } elseif ($payin4Min > $config['payin4']["maximumAmount"]) {
                $errorPayin4Min = true;
                $isError = true;
            } elseif (
                $payin4Min > $payin4Max &&
                $payin4Max <= $config['payin4']["maximumAmount"]
            ) {
                $errorPayin4Min = true;
                $isError = true;
            }

            //max value
            if ($config['payin4']["maximumAmount"] < $payin4Max) {
                $errorPayin4Max = true;
                $this->configWriter->save(
                    'scalapay/payin4/restrictions/max_amount',
                    $config['payin4']["maximumAmount"]
                );
                $isError = true;
            } elseif ($payin4Max < $config['payin4']["minimumAmount"]) {
                $errorPayin4Max = true;
                $isError = true;
            } elseif (
                $payin4Max < $payin4Min &&
                $payin4Min >= $config['payin4']["minimumAmount"]
            ) {
                $errorPayin4Max = true;
                $isError = true;
            }
            if ($isError == true) {
                if ($errorPayin4Min == true || $errorPayin4Max == true) {
                    $this->messageManager->addError($errorPayin4Init);
                }
            }
        }

        ////pay later
        $errorPaylaterMin = false;
        $errorPaylaterMax = false;
        $errorPaylaterInit = null;
        if (
            isset($config['paylater']["minimumAmount"]) &&
            isset($paylaterMin) &&
            isset($config['paylater']["maximumAmount"]) &&
            isset($paylaterMax)
        ) {
            $errorPaylaterInit = __(
                "Scalapay - pay later amount should be between %1 and %2.",
                $config['paylater']["minimumAmount"],
                $config['paylater']["maximumAmount"]
            );
            if ($config['paylater']["minimumAmount"] > $paylaterMin) {
                $errorPaylaterMin = true;
                $isError = true;
            } elseif ($paylaterMin >  $config['paylater']["maximumAmount"]) {
                $errorPaylaterMin = true;
                $isError = true;
            } elseif (
                $paylaterMin > $paylaterMax &&
                $paylaterMax <= $config['paylater']["maximumAmount"]
            ) {
                $errorPaylaterMin = true;
                $isError = true;
            }

            //max value
            if ($config['paylater']["maximumAmount"] < $paylaterMax) {
                $errorPaylaterMax = true;
                $this->configWriter->save(
                    'scalapay/paylater/restrictions/max_amount',
                    $config['paylater']["maximumAmount"]
                );
                $isError = true;
            } elseif ($paylaterMax <  $config['paylater']["minimumAmount"]) {
                $errorPaylaterMax = true;
                $isError = true;
            } elseif (
                $paylaterMax < $paylaterMin &&
                $paylaterMin >= $config['paylater']["minimumAmount"]
            ) {
                $errorPaylaterMax = true;
                $isError = true;
            }
            if ($isError == true) {
                if ($errorPaylaterMin == true || $errorPaylaterMax == true) {
                    $this->messageManager->addError($errorPaylaterInit);
                }
            }
        }

        // diabling the payment methods:
        $this->cleanCacheByScalapay();

        return $isError;
    }

    public function cleanCacheByScalapay()
    {
        $_types = [
            'config',
            'full_page'
        ];
        foreach ($_types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }

    /**
     * Save configuration
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $isScalapayError = 0;
        if ($this->getRequest()->getParam('section') == 'scalapay') {
            $isScalapayError = $this->saveScalapayConfiguration();
        }
        if (!$isScalapayError) {
            parent::execute();
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath(
            'adminhtml/system_config/edit',
            [
                '_current' => ['section', 'website', 'store'],
                '_nosid' => true
            ]
        );
    }
}
