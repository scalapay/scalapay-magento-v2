<?php

namespace Scalapay\Scalapay\Controller\Adminhtml\Index;

use Scalapay\Scalapay\Helper\Data as HelperData;

class Config extends \Magento\Backend\App\Action
{
    protected $helper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        HelperData $helperData,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->helper = $helperData;
        $this->resultJsonFactory = $resultJsonFactory;

        return parent::__construct($context);
    }

    /**
     * Get Ppup Image
     *
     * @return string
     *
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $config = $this->helper->getScalapayConfigV3();
            $return = $config;

            return $result->setData($return);
        }
    }
}
