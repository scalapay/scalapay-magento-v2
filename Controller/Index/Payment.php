<?php

namespace Scalapay\Scalapay\Controller\Index;

use Scalapay\Scalapay\Helper\Data as HelperData;

class Payment extends \Magento\Framework\App\Action\Action
{
    protected $_helper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        HelperData $helperData
    ) {
        $this->_helper = $helperData;

        return parent::__construct($context);
    }
    /**
     * Get Capture Payment
     *
     * @return array
     *
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $orderToken = $params["orderToken"];
        $status = $params["status"];
        if ($status == "SUCCESS") {
            $responce = $this->_helper->scalapayCapture($orderToken);
        }
    }
}
