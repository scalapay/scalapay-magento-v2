<?php

namespace Scalapay\Scalapay\Controller\Index;

use Scalapay\Scalapay\Helper\Data as HelperData;

class Reload extends \Magento\Framework\App\Action\Action
{
    protected $_helper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        HelperData $helperData,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->_helper = $helperData;
        $this->resultJsonFactory = $resultJsonFactory;

        return parent::__construct($context);
    }

    /**
     * Get Ppup Image
     *
     * @return string
     *
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();

        if ($this->getRequest()->isAjax()) {
            $method = $this->getRequest()->getPost('method');
            $amount = $this->getRequest()->getPost('amount');
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $configData = $this->_helper->getScalapayConfig();
            $numberOfPayments = $configData["numberOfPayments"];
            $getCurrentQuote = $om->create('Magento\Checkout\Model\Cart')->getQuote();

            $grandTotal = $getCurrentQuote->getGrandTotal();
            $grandTotal += (int)$amount;
            $scalapay_product_price    = round($grandTotal / $numberOfPayments, 2);
            $scalapay_product_price = $this->_helper->getScalapayFormatedPrice($scalapay_product_price);
            $return = [$scalapay_product_price];

            return $result->setData($return);
        }
    }
}
