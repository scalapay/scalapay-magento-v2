<?php

namespace Scalapay\Scalapay\Controller\Index;

use Scalapay\Scalapay\Helper\Data as HelperData;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_url;
    protected $_helper;
    protected $_messageManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        HelperData $helperData,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_helper = $helperData;
        $this->_url = $url;
        $this->_messageManager = $messageManager;

        return parent::__construct($context);
    }
    /**
     * Get Order Checkout Url
     *
     * @return string
     *
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $res = $this->_helper->scalapayOrder($params);
        if (isset($res["error"]) && $res["error"] == 1) {
            $error_message = "";
            if (isset($res["message"])) {
                $error_message = $res["message"];
            }
            $this->cancelOrder($error_message);
        } elseif (isset($res["error"]) && $res["error"] == 0) {
            if (isset($res["message"])) {
                $url = $this->_helper->ctrim($res["message"]);
                $this->_redirect($url);
            }
        } else {
            $scalapay_failed_payment_error = $this->_helper->getConfigData("scalapay_error_message");

            if (isset($scalapay_failed_payment_error) && $scalapay_failed_payment_error != "") {
                $this->_messageManager->addNotice($scalapay_failed_payment_error);
            } else {
                $this->_messageManager->addNotice(__('The payment was not successful. Choose another payment method'));
            }

            $cartUrl = $this->_url->getUrl('checkout/cart');
            $this->_redirect($cartUrl);
        }

        //return $this;
    }

    public function cancelOrder($message = null)
    {
        $scalapay_failed_payment_error = $this->_helper->getConfigData("scalapay_error_message");
        if ($message !== null) {
            $this->_helper->log($message);
            $this->_messageManager->addError($message);
        } elseif (isset($scalapay_failed_payment_error) && $scalapay_failed_payment_error != "") {
            $this->_messageManager->addError($scalapay_failed_payment_error);
        } else {
            $this->_messageManager->addError('The payment was not successful. Choose another payment method');
        }

        $cartUrl = $this->_url->getUrl('checkout/cart');
        $this->_redirect($cartUrl);
    }
}
