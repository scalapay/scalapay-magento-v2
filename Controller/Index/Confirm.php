<?php

namespace Scalapay\Scalapay\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Scalapay\Scalapay\Helper\Data as HelperData;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\ValidatorException;

class Confirm extends \Magento\Framework\App\Action\Action
{
    protected $_helper;
    protected $_checkoutSession;
    protected $_url;
    protected $_orderCollectionFactory;
    protected $_invoiceService;
    protected $_transaction;
    protected $_resultFactory;
    protected $_eventManager;
    protected $_quoteManagement;
    protected $_customerSession;
    protected $_customerRepository;
    protected $_objectCopyService;
    protected $_dataObjectHelper;
    protected $_messageManager;
    protected $_quoteRepository;
    protected $_transactionBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        HelperData $helperData,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\UrlInterface $url,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\DataObject\Copy $objectCopyService,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        BuilderInterface $transactionBuilder
    ) {
        $this->_helper = $helperData;
        $this->_checkoutSession = $checkoutSession;
        $this->_url = $url;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->_resultFactory = $context->getResultFactory();
        $this->_eventManager = $eventManager;
        $this->_quoteManagement = $quoteManagement;
        $this->_customerSession = $customerSession;
        $this->_customerRepository = $customerRepository;
        $this->_objectCopyService = $objectCopyService;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_messageManager = $messageManager;
        $this->_quoteRepository = $quoteRepository;
        $this->_transactionBuilder = $transactionBuilder;

        return parent::__construct($context);
    }
    /**
     * Get Confirm Payment
     *
     * @return redirect to sucess/errro page
     *
     */
    public function execute()
    {
        $this->_helper->log("Info: Scalapay confirm action.");
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $scalapay_failed_payment_error = $this->_helper->getConfigData("scalapay_error_message");

        if (isset($scalapay_failed_payment_error) && $scalapay_failed_payment_error != "") {
            $error_message = $scalapay_failed_payment_error;
        } else {
            $error_message = "The payment was not successful. Choose another payment method";
        }

        $params = $this->getRequest()->getParams();
        $status = "";
        $orderToken = "";
        $paymentMethodName = "";

        if (isset($params["orderToken"])) {
            $orderToken = $params["orderToken"];
        }
        if (isset($params["orderToken"])) {
            $status = $params["status"];
        }
        if (isset($params["paymentmethod"])) {
            $paymentMethodName = $params["paymentmethod"];
        }

        $this->_helper->log("orderToken: " . $orderToken);
        $this->_helper->log("Status: " . $status);
        $quote = $this->getQuote();
        //check if quote id is null
        if (($quote == null || $quote->getId() == null) && isset($params["cart_id"])) {
            $this->_helper->log("loading from quoteRepository.");
            $cartId = $params["cart_id"];
            $quote = $this->_quoteRepository->getActive($cartId);
        }
        $quoteIncrementId = $quote->getId();
        $grandTotal = $quote->getGrandTotal();

        $this->_helper->log("Quote Increment Id: " . $quoteIncrementId);
        $this->_helper->log("Grand Total: " . $grandTotal);

        $cart_id_return = "";
        $order_total_return = 0;

        if (isset($params["cart_id"])) {
            $cart_id_return = $params["cart_id"];
        }

        if (isset($params["total"])) {
            $order_total_return = $params["total"];
        }

        $proceed_for_order = true;

        //extra check if user has the same cart details for which he/she paid the amount
        if ($cart_id_return == "" || $order_total_return == 0) {
            $proceed_for_order = false;
            $this->_helper->log("Proceed_for_order: No 1");
        }

        if ($cart_id_return != "" && $order_total_return > 0) {
            if (
                $cart_id_return != $quoteIncrementId ||
                !$this->_helper->epsilonCalculation($grandTotal, $order_total_return)
            ) {
                $proceed_for_order = false;
                $this->_helper->log("Proceed_for_order: No 2");
            }
        }

        if ($proceed_for_order) {
            $this->_helper->log("Proceed_for_order: Yes 1");
            $this->_helper->log("cehckout method: " . $quote->getCheckoutMethod());

            if (
                $quote->getCheckoutMethod() === \Magento\Checkout\Model\Type\Onepage::METHOD_GUEST ||
                $quote->getCheckoutMethod() === null
            ) {
                $this->_helper->log("Customer Type - Guest: " . \Magento\Checkout\Model\Type\Onepage::METHOD_GUEST);
                $this->_prepareGuestQuote();
            } elseif (
                $quote->getCheckoutMethod() === \Magento\Checkout\Model\Type\Onepage::METHOD_REGISTER
            ) {
                $this->_helper->log(
                    "Customer Type - new registered customer: " . \Magento\Checkout\Model\Type\Onepage::METHOD_REGISTER
                );
                $this->_prepareNewCustomerQuote();
            } else {
                $this->_helper->log(
                    "Customer Type - registered customer: " . \Magento\Checkout\Model\Type\Onepage::METHOD_REGISTER
                );
                $this->_prepareCustomerQuote();
            }
            try {
                $this->_helper->log("paymentMethodName: " . $paymentMethodName);
                if ($quote->getPayment()->getMethod() == "" && ($paymentMethodName == "scalapay" || $paymentMethodName == "payin4" || $paymentMethodName == "paylater")) {
                    $quote->getPayment()->setMethod($paymentMethodName)->save();
                    $quote->setInventoryProcessed(false); //not effetc inventory
                    // Set Sales Order Payment
                    $this->_helper->log("paymentMethodName updated: " . $paymentMethodName);

                } else {
                    $quote->setInventoryProcessed(false); //not effetc inventory
                }
            } catch (LocalizedException $e) {
                $this->_helper->log("error: LocalizedException - quote - import payment data.");
                $this->cancelOrder($e->getMessage());
                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart');

                return $resultRedirect;
            } catch (\Exception $e) {
                $this->_helper->log("error: - quote - import payment data.");
                $this->cancelOrder($e->getMessage());
                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart');

                return $resultRedirect;
            }
        }

        $order_status_final = $this->_helper->getConfigData("order_status");
        //sales_model_service_quote_submit_before
        $this->_checkoutSession->setData('scalapay_order_place_before', 'proper');

        if (isset($order_status_final) && $order_status_final != "" && $orderToken != "") {
            $duplicatedOrders = $this->_orderCollectionFactory->create()
                ->addAttributeToSelect('entity_id')
                ->addFieldToFilter('order_token', ['eq' => $orderToken])
                ->addFieldToFilter('status', ['eq' => $order_status_final]);

            if ($duplicatedOrders->getSize() > 0) {
                $proceed_for_order = false;
                $this->_helper->log("Scalapay: confirm action - duplicated size : " . $duplicatedOrders->getSize());
                $this->_helper->log("Scalapay: confirm action - duplicated order: " . $orderToken);
                $this->_redirect('checkout/onepage/success', array('_secure' => true));
                return $this;
            }
        }

        /********if confirm responce from scalapay*******/
        if (isset($status) && $status == "SUCCESS" && isset($orderToken) && $proceed_for_order) {
            /********Set order payment status confirm*******/
            $this->_helper->log("Status: SUCCESS");
            $this->_eventManager->dispatch('checkout_submit_before', ['quote' => $quote]);
            try {
                $this->_helper->log("Confirm Action : Billing data");
                $this->_helper->log("Collect total saving.");
                $quote->collectTotals()->save();
                $this->_helper->log("Submitting Quote to convert order.");
                $order = $this->_quoteManagement->submit($quote);
                $this->_helper->log("After submitting Quote to convert order.");
            } catch (LocalizedException $e) {
                $this->_helper->log("error: LocalizedException - while converting quote.");
                $this->_helper->log($e->getMessage());
                try {
                    $this->_helper->log("Confirm Action : 2nd try Billing and Shipping data");
                    $region_id = $quote->getBillingAddress()->getRegionId();
                    $state = $quote->getBillingAddress()->getRegion();
                    $country_code = $quote->getBillingAddress()->getCountryId();
                    $region_id = $this->_helper->processRegionId($region_id, $state, $country_code);
                    if ($region_id != "") {
                        $quote->getBillingAddress()->setRegionId($region_id);
                    }

                    //validation start
                    //checks for registered customer - validate customer data
                    $validationErrors = [];
                    $quote->getBillingAddress()->setStoreId($quote->getStoreId());
                    $validationResult = $quote->getBillingAddress()->validate();
                    $payload = $this->_helper->scalapayGetPayloadData($orderToken);
                    $this->_helper->log("VAlidaation start for: Billing data");
                    if ($validationResult !== true) {
                        //billing validation failed.
                        $this->_helper->log("Billing data Validation failed");

                        if ($payload["error"] === 0 && $payload['billing'] !== null) {
                            $name = explode(" ", $payload['billing']['name']);

                            if ($name[0] !== null && $quote->getBillingAddress()->getFirstName() == "") {
                                $fname = $name[0];
                                $quote->getBillingAddress()->setFirstName($fname);
                            }
                            if ($name[1] !== null && $quote->getBillingAddress()->getLastName() == "") {
                                $lname = $name[1];
                                $quote->getBillingAddress()->setLastName($lname);
                            }
                            if (
                                $payload['consumer']['email'] !== null &&
                                $quote->getBillingAddress()->getEmail() == ""
                            ) {
                                $quote->getBillingAddress()->setEmail($payload['consumer']['email']);
                            }
                            if (
                                $payload['billing']['line1'] !== null &&
                                $quote->getBillingAddress()->getStreetLine(1) == ""
                            ) {
                                $quote->getBillingAddress()->setStreet($payload['billing']['line1']);
                            }
                            if (
                                $payload['billing']['suburb'] !== null &&
                                $quote->getBillingAddress()->getCity() == ""
                            ) {
                                $quote->getBillingAddress()->setCity($payload['billing']['suburb']);
                            }
                            if (
                                $payload['billing']['suburb'] !== null &&
                                $quote->getBillingAddress()->getRegion() == ""
                            ) {
                                $quote->getBillingAddress()->setRegion($payload['billing']['suburb']);
                            }
                            if (
                                $payload['billing']['postcode'] !== null &&
                                $quote->getBillingAddress()->getPostcode() == ""
                            ) {
                                $quote->getBillingAddress()->setPostcode($payload['billing']['postcode']);
                            }
                            if (
                                $payload['billing']['countryCode'] !== null &&
                                $quote->getBillingAddress()->getCountryId() == ""
                            ) {
                                $quote->getBillingAddress()->setCountryId($payload['billing']['countryCode']);
                            }
                            if (
                                $payload['billing']['phoneNumber'] !== null &&
                                $quote->getBillingAddress()->getTelephone() == ""
                            ) {
                                $quote->getBillingAddress()->setTelephone($payload['billing']['phoneNumber']);
                            }
                        }
                    }
                    $quote->getBillingAddress()->setSaveInAddressBook(1)->save();

                    $region_id_shipping = $quote->getShippingAddress()->getRegionId();
                    $state_shipping = $quote->getShippingAddress()->getRegion();
                    $country_code_shipping = $quote->getShippingAddress()->getCountryId();
                    $region_id_shipping = $this->_helper->processRegionId(
                        $region_id_shipping,
                        $state_shipping,
                        $country_code_shipping
                    );
                    if ($region_id_shipping != "") {
                        $quote->getShippingAddress()->setRegionId($region_id_shipping);
                    }

                    if (!$quote->isVirtual()) {
                        $quote->getShippingAddress()->setStoreId($quote->getStoreId());
                        $validationResultShipping = $quote->getShippingAddress()->validate();

                        if ($validationResultShipping !== true) {
                            $this->_helper->log("Shipping data Validation failed");
                            //shipping validation failed.
                            if ($payload["error"] === 0 && $payload['shipping'] !== null) {
                                $name = explode(" ", $payload['shipping']['name']);

                                if (
                                    $name[0] !== null &&
                                    $quote->getShippingAddress()->getFirstName() == ""
                                ) {
                                    $fname = $name[0];
                                    $quote->getShippingAddress()->setFirstName($fname);
                                }
                                if (
                                    $name[1] !== null &&
                                    $quote->getShippingAddress()->getLastName() == ""
                                ) {
                                    $lname = $name[1];
                                    $quote->getShippingAddress()->setLastName($lname);
                                }

                                if (
                                    $payload['consumer']['email'] !== null &&
                                    $quote->getShippingAddress()->getEmail() == ""
                                ) {
                                    $quote->getShippingAddress()->setEmail($payload['consumer']['email']);
                                }
                                if (
                                    $payload['shipping']['line1'] !== null &&
                                    $quote->getShippingAddress()->getStreetLine(1) == ""
                                ) {
                                    $quote->getShippingAddress()->setStreet($payload['shipping']['line1']);
                                }
                                if (
                                    $payload['shipping']['suburb'] !== null &&
                                    $quote->getShippingAddress()->getCity() == ""
                                ) {
                                    $quote->getShippingAddress()->setCity($payload['shipping']['suburb']);
                                }
                                if (
                                    $payload['shipping']['suburb'] !== null &&
                                    $quote->getShippingAddress()->getRegion() == ""
                                ) {
                                    $quote->getShippingAddress()->setRegion($payload['shipping']['suburb']);
                                }
                                if (
                                    $payload['shipping']['postcode'] !== null &&
                                    $quote->getShippingAddress()->getPostcode() == ""
                                ) {
                                    $quote->getShippingAddress()->setPostcode($payload['shipping']['postcode']);
                                }
                                if (
                                    $payload['shipping']['countryCode'] !== null &&
                                    $quote->getShippingAddress()->getCountryId() == ""
                                ) {
                                    $quote->getShippingAddress()->setCountryId($payload['shipping']['countryCode']);
                                }
                                if (
                                    $payload['shipping']['phoneNumber'] !== null &&
                                    $quote->getShippingAddress()->getTelephone() == ""
                                ) {
                                    $quote->getShippingAddress()->setTelephone($payload['shipping']['phoneNumber']);
                                }
                            }
                        }
                    }

                    /// end validation

                    $quote->getShippingAddress()->setSaveInAddressBook(1)->save();
                    $this->_helper->log("Submitting Quote 2nd time to convert order.");
                    if ($payload['consumer']['email'] !== null && $quote->getCustomerEmail() == "") {
                        $this->_helper->log("customer email is empty.");
                        $quote->setCustomerEmail($payload['consumer']['email']);
                    }
                    $order = $this->_quoteManagement->submit($quote);
                } catch (LocalizedException $e) {
                    $this->_helper->log("error: LocalizedException - while converting quote 2nd time.");
                    $this->cancelOrder($e->getMessage());
                    $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultRedirect->setPath('checkout/cart');

                    return $resultRedirect;
                } catch (\Exception $e) {
                    $this->_helper->log("error: while converting quote 2nd time.");
                    $this->cancelOrder($e->getMessage());
                    $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultRedirect->setPath('checkout/cart');

                    return $resultRedirect;
                }
            } catch (NoSuchEntityException $e) {
                $this->_helper->log("NoSuchEntity Exception: while converting quote: ");
                $this->_helper->log($e->getMessage());
                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart');

                return $resultRedirect;
            } catch (CouldNotSaveException $e) {
                $this->_helper->log("CouldNotSaveException: while converting quote.");
                $this->cancelOrder($e->getMessage());
                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart');

                return $resultRedirect;
            } catch (AuthorizationException $e) {
                $this->_helper->log("AuthorizationException: while converting quote.");
                $this->cancelOrder($e->getMessage());
                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart');

                return $resultRedirect;
            } catch (NotFoundException $e) {
                $this->_helper->log("NotFoundException: while converting quote.");
                $this->cancelOrder($e->getMessage());
                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart');

                return $resultRedirect;
            } catch (ValidatorException $e) {
                $this->_helper->log("ValidatorException: while converting quote.");
                $this->cancelOrder($e->getMessage());
                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart');

                return $resultRedirect;
            } catch (\Exception $e) {
                $this->_helper->log("error: while converting quote.");
                $this->cancelOrder($e->getMessage());
                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('checkout/cart');

                return $resultRedirect;
            }

            //checking order total is matched with Scalapay order total
            if ($this->_helper->epsilonCalculation($order->getGrandTotal(), $order_total_return)) {

                /******Set order payment status charged and complete order***/
                $this->_helper->log("Can Invoice: before");
                if ($order->canInvoice()) {
                    /********Capture Payment*******/
                    $this->_helper->log("Scalapay Capture before.");
                    $captureResponse = $this->_helper->scalapayCapture(
                        $orderToken,
                        $order->getIncrementId()
                    );

                    if (isset($captureResponse["error"]) && $captureResponse["error"] == 1) {
                        $this->cancelOrder($captureResponse["message"]);
                        $this->_helper->log("Capture Response: error 1");
                    } else {

                        /********Get payment status*******/
                        if (isset($captureResponse["message"]) && $captureResponse["message"] == "APPROVED") {
                            $this->_helper->log("Capture Response: APPROVED");
                            $invoice = $this->_invoiceService->prepareInvoice($order);
                            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                            $invoice->register();
                            $invoice->setTransactionId($orderToken);
                            $invoice->save();
                            $this->_helper->log("Invoice Saved");
                            $transactionSave = $this->_transaction->addObject(
                                $invoice
                            )
                            ->addObject($invoice->getOrder());

                            $transactionSave->save();
                            $this->_helper->log("Invoice Created");
                            /// creating payment transaction
                            $payment = $order->getPayment();
                            $payment->setLastTransId($orderToken);
                            $payment->setTransactionId($orderToken);

                            $trans = $this->_transactionBuilder;
                            $formatedPrice = $order->getBaseCurrency()->formatTxt(
                                $order->getGrandTotal()
                            );
                            $message = __('Transaction Amount: %1.', $formatedPrice);
                            $transaction = $trans->setPayment($payment)
                            ->setOrder($order)
                            ->setTransactionId($orderToken)
                            ->setAdditionalInformation(
                                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => []]
                            )
                            ->setFailSafe(true)
                            //build method creates the transaction and returns the object
                            ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);
                            $payment->addTransactionCommentsToOrder(
                                $transaction,
                                $message
                            );
                            $payment->setParentTransactionId(null);
                            $payment->save();
                            $transaction->save();
                            /////////////// End transaction code

                            //send notification code
                            $order->addStatusHistoryComment(
                                __("Payment reference order token " . $orderToken)
                            )
                                ->setIsCustomerNotified(true)
                                ->save();

                            $order_status_final = $this->_helper->getConfigData("order_status");

                            if (isset($order_status_final) && $order_status_final != "") {
                                $order->setStatus($order_status_final);
                            }

                            $order->setOrderToken($orderToken);
                            $order->save();
                            $this->_helper->log("Order Created");
                            $this->_helper->log("Order IncreamentId: " . $order->getIncrementId());
                            $this->_checkoutSession->setLastQuoteId($quote->getId());
                            $this->_checkoutSession->setLastSuccessQuoteId($quote->getId());
                            $this->_checkoutSession->setLastOrderId($order->getId());
                            $this->_checkoutSession->setLastRealOrderId($order->getIncrementId());
                            $this->_checkoutSession->setLastOrderStatus($order->getStatus());

                            try {
                                $this->_helper->log("checkout_submit_all_after event start.");
                                $this->_eventManager->dispatch(
                                    'checkout_submit_all_after',
                                    ['order' => $order, 'quote' => $quote]
                                );
                            } catch (LocalizedException $e) {
                                $this->_helper->log(
                                    "error: LocalizedException - while checkout_submit_all_after event."
                                );
                                $this->_helper->log($e->getMessage());
                                $this->cancelOrder($e->getMessage());
                                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                                $resultRedirect->setPath('checkout/cart');

                                return $resultRedirect;
                            } catch (NoSuchEntityException $e) {
                                $this->_helper->log(
                                    "error: NoSuchEntityException - while checkout_submit_all_after event."
                                );
                                $this->_helper->log($e->getMessage());

                                return $resultRedirect;
                            } catch (\Exception $e) {
                                $this->_helper->log("error: while checkout_submit_all_after event.");
                                $this->_helper->log($e->getMessage());
                                $this->cancelOrder($e->getMessage());
                                $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                                $resultRedirect->setPath('checkout/cart');

                                return $resultRedirect;
                            }

                            $this->_helper->log("checkout_submit_all_after event end");
                            $this->_helper->log("redirecting to success page");
                            $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
                            $resultRedirect->setPath('checkout/onepage/success');

                            return $resultRedirect;
                        } else {
                            $this->_helper->log("Scalapay order: failed 1 - capture not APPROVED");
                            $this->cancelOrder();
                        }
                    }
                } else {
                    $this->_helper->log("Scalapay order: failed to invoice");
                    $this->cancelOrder();
                }
            } else {
                $this->_helper->log("Scalapay order: Scalapay order total is not matched.");
                $this->_helper->log("Magento  order total: " . $order->getGrandTotal());
                $this->_helper->log("Scalapay order total: " . $order_total_return);
                $this->cancelOrder("The payment was not successful. Please try again.");
            }
        } else {
            $this->_helper->log("Scalapay order: failed 2");
            $this->cancelOrder();
        }

        $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('checkout/onepage/success');

        return $resultRedirect;
    }

    public function cancelOrder($message = null)
    {
        if ($message === null) {
            $paymentError = $this->_helper->getConfigData("scalapay_error_message");
            if (isset($paymentError) && $paymentError != "") {
                $this->_messageManager->addError($paymentError);
            } else {
                $this->_messageManager->addError('The payment was not successful. Choose another payment method');
            }
        } else {
            $this->_helper->log("Cacnel Order: ");
            $this->_helper->log($message);
            $this->_messageManager->addError($message);
        }

        $cartUrl = $this->_url->getUrl('checkout/cart');
        $this->_redirect($cartUrl);
    }

    /**
     * Prepare quote for guest checkout order submit
     *
     * @return $this
     */
    protected function _prepareGuestQuote()
    {
        $quote = $this->getQuote();
        $quote->setCustomerId(null)
            ->setCustomerEmail($quote->getBillingAddress()->getEmail())
            ->setCustomerIsGuest(true)
            ->setCustomerFirstname($quote->getBillingAddress()->getFirstname())
            ->setCustomerLastname($quote->getBillingAddress()->getLastname())
            ->setCustomerGroupId(GroupInterface::NOT_LOGGED_IN_ID);
        return $this;
    }

    /**
     * Prepare quote for customer registration and customer order submit
     *
     * @return void
     */
    protected function _prepareNewCustomerQuote()
    {
        $quote = $this->getQuote();
        $billing = $quote->getBillingAddress();
        $shipping = $quote->isVirtual() ? null : $quote->getShippingAddress();

        $customer = $quote->getCustomer();
        $customerBillingData = $billing->exportCustomerAddress();
        $dataArray = $this->_objectCopyService->getDataFromFieldset('checkout_onepage_quote', 'to_customer', $quote);
        $this->_dataObjectHelper->populateWithArray(
            $customer,
            $dataArray,
            \Magento\Customer\Api\Data\CustomerInterface::class
        );
        $quote->setCustomer($customer)->setCustomerId(true);

        $customerBillingData->setIsDefaultBilling(true);

        if ($shipping) {
            if (!$shipping->getSameAsBilling()) {
                $customerShippingData = $shipping->exportCustomerAddress();
                $customerShippingData->setIsDefaultShipping(true);
                $shipping->setCustomerAddressData($customerShippingData);
                // Add shipping address to quote since customer Data Object does not hold address information
                $quote->addCustomerAddress($customerShippingData);
            } else {
                $shipping->setCustomerAddressData($customerBillingData);
                $customerBillingData->setIsDefaultShipping(true);
            }
        } else {
            $customerBillingData->setIsDefaultShipping(true);
        }
        $billing->setCustomerAddressData($customerBillingData);
        // TODO : Eventually need to remove this legacy hack
        // Add billing address to quote since customer Data Object does not hold address information
        $quote->addCustomerAddress($customerBillingData);
        $this->getCustomerSession()->loginById($customer->getId());
    }

    /**
     * Prepare quote for customer order submit
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function _prepareCustomerQuote()
    {
        $quote = $this->getQuote();
        $billing = $quote->getBillingAddress();
        $shipping = $quote->isVirtual() ? null : $quote->getShippingAddress();

        $customer = $this->_customerRepository->getById($this->getCustomerSession()->getCustomerId());
        $hasDefaultBilling = (bool)$customer->getDefaultBilling();
        $hasDefaultShipping = (bool)$customer->getDefaultShipping();

        if (
            $shipping && !$shipping->getSameAsBilling() &&
            (!$shipping->getCustomerId() || $shipping->getSaveInAddressBook())
        ) {
            $shippingAddress = $shipping->exportCustomerAddress();
            if (!$hasDefaultShipping) {
                //Make provided address as default shipping address
                $shippingAddress->setIsDefaultShipping(true);
                $hasDefaultShipping = true;
            }
            $quote->addCustomerAddress($shippingAddress);
            $shipping->setCustomerAddressData($shippingAddress);
        }

        if (!$billing->getCustomerId() || $billing->getSaveInAddressBook()) {
            $billingAddress = $billing->exportCustomerAddress();
            if (!$hasDefaultBilling) {
                //Make provided address as default shipping address
                if (!$hasDefaultShipping) {
                    //Make provided address as default shipping address
                    $billingAddress->setIsDefaultShipping(true);
                }
                $billingAddress->setIsDefaultBilling(true);
            }
            $quote->addCustomerAddress($billingAddress);
            $billing->setCustomerAddressData($billingAddress);
        }
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     * @codeCoverageIgnore
     */
    public function getCheckout()
    {
        return $this->_checkoutSession;
    }

    /**
     * Quote object getter
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->_checkoutSession->getQuote();
    }

    /**
     * Get customer session object
     *
     * @return \Magento\Customer\Model\Session
     * @codeCoverageIgnore
     */
    public function getCustomerSession()
    {
        return $this->_customerSession;
    }
}
