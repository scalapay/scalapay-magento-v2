<?php

namespace Scalapay\Scalapay\Controller\Index;

class Cancel extends \Magento\Framework\App\Action\Action
{
    protected $_helper;
    protected $_messageManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Scalapay\Scalapay\Helper\Data $helper,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_helper = $helper;
        $this->_messageManager = $messageManager;

        return parent::__construct($context);
    }
    /**
     * Get Ppup Image
     *
     * @return string
     *
     */
    public function execute()
    {
        $scalapay_failed_payment_error = $this->_helper->getConfigData("scalapay_error_message");

        if (isset($scalapay_failed_payment_error) && $scalapay_failed_payment_error != "") {
            $this->_messageManager->addNotice($scalapay_failed_payment_error);
        } else {
            $this->_messageManager->addNotice(__('The payment was not successful. Choose another payment method'));
        }

        $this->_redirect('checkout');
    }
}
